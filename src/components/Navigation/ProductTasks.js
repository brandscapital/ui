import React from 'react'
import { Header } from 'semantic-ui-react'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productTasksView = {
  id: 'product/tasks',
  type: 'product',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/tasks:header',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/tasks:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      items: [
        {
          type: 'SectionHeading',
          text: 'Tasks',
          id: 'tasks-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-tasks',
          before: ListIcon,
          text: 'List & Manage',
          to: '/tasks'
        }
      ]
    }
  ]
}

export default productTasksView
