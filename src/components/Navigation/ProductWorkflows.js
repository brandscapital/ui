import React from 'react'
import { Header } from 'semantic-ui-react'
import AddIcon from '@atlaskit/icon/glyph/add-item'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productWorkflowsView = {
  id: 'product/workflows',
  type: 'product',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/workflows:header',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/workflows:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      items: [
        {
          type: 'SectionHeading',
          text: 'Workflows',
          id: 'workflows-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-workflows',
          before: ListIcon,
          text: 'List & Manage',
          to: '/workflows'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-workflow',
          before: AddIcon,
          text: 'Create',
          subText: 'New Workflow',
          to: '/workflows/new'
        }
      ]
    }
  ]
}

export default productWorkflowsView
