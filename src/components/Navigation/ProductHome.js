import React from 'react'
import { Header } from 'semantic-ui-react'
import DashboardIcon from '@atlaskit/icon/glyph/dashboard'
import DocumentsIcon from '@atlaskit/icon/glyph/documents'
import DataIcon from '@atlaskit/icon/glyph/backlog'
import PeopleIcon from '@atlaskit/icon/glyph/people'
import RolesIcon from '@atlaskit/icon/glyph/lock'
import TaskIcon from '@atlaskit/icon/glyph/task'
import TasksIcon from '@atlaskit/icon/glyph/subtask'
import WorkflowsIcon from '@atlaskit/icon/glyph/bitbucket/forks'
import LinkItem from './LinkItem'

const productHomeView = {
  id: 'product/home',
  type: 'product',
  spacing: 'compact',
  getItems: () => [
    {
      type: 'HeaderSection',
      spacing: 'compact',
      id: 'product/home:header',
      items: [
        {
          id: 'header',
          spacing: 'compact',
          type: 'InlineComponent',
          component: ({ className }) => (
            <div className={className} style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        }
      ]
    },
    {
      type: 'MenuSection',
      spacing: 'compact',
      nestedGroupKey: 'menu',
      id: 'product/home:menu',
      parentId: null,
      items: [
        {
          type: 'InlineComponent',
          spacing: 'compact',
          component: LinkItem,
          id: 'dashboard',
          before: DashboardIcon,
          text: 'Dashboard',
          to: '/'
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          before: TaskIcon,
          id: 'my-tasks',
          text: 'My Tasks',
          to: '/my-tasks',
          component: LinkItem
        },
        {
          id: 'separator',
          spacing: 'compact',
          type: 'Separator'
        },
        {
          before: DocumentsIcon,
          spacing: 'compact',
          id: 'documents',
          text: 'Documents',
          type: 'GoToItem',
          goTo: 'product/documents'
        },
        {
          before: TasksIcon,
          spacing: 'compact',
          id: 'tasks',
          text: 'Tasks',
          goTo: 'product/tasks',
          type: 'GoToItem'
        },
        {
          before: WorkflowsIcon,
          spacing: 'compact',
          id: 'workflows',
          text: 'Workflows',
          goTo: 'product/workflows',
          type: 'GoToItem'
        },
        {
          before: DataIcon,
          id: 'stakeholders',
          text: 'Records',
          goTo: 'product/records',
          type: 'GoToItem'
        },
        {
          before: PeopleIcon,
          spacing: 'compact',
          id: 'users',
          text: 'Users',
          goTo: 'product/users',
          type: 'GoToItem'
        },
        {
          before: RolesIcon,
          spacing: 'compact',
          id: 'roles',
          text: 'Roles',
          type: 'Item',
          goTo: 'product/roles'
        }
      ]
    }
  ]
}

export default productHomeView
