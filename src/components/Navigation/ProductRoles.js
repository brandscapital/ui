import React from 'react'
import { Header } from 'semantic-ui-react'
import AddIcon from '@atlaskit/icon/glyph/add-item'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productRolesView = {
  id: 'product/roles',
  type: 'product',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/roles:header',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/roles:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      items: [
        {
          type: 'SectionHeading',
          text: 'Roles',
          id: 'roles-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-roles',
          before: ListIcon,
          text: 'List & Manage',
          to: '/roles'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-role',
          before: AddIcon,
          text: 'Create',
          subText: 'New Role',
          to: '/roles/new'
        },
        {
          type: 'SectionHeading',
          text: 'Permissions',
          id: 'permissions-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-permissions',
          before: ListIcon,
          text: 'List & Manage',
          to: '/permissions'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-permissions',
          before: AddIcon,
          text: 'Create',
          subText: 'New Permission',
          to: '/permissions/new'
        }
      ]
    }
  ]
}

export default productRolesView
