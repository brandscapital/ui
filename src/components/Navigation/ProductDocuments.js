import React from 'react'
import { Header } from 'semantic-ui-react'
import AddIcon from '@atlaskit/icon/glyph/add-item'
import VidPause from '@atlaskit/icon/glyph/vid-pause'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productDocumentsView = (createCollection, goToThread) => ({
  id: 'product/documents',
  type: 'product',
  spacing: 'compact',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/documents:header',
      spacing: 'compact',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          spacing: 'compact',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          spacing: 'compact',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/documents:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      spacing: 'compact',
      items: [
        {
          type: 'SectionHeading',
          text: 'Collections',
          spacing: 'compact',
          id: 'collections-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          spacing: 'compact',
          id: 'list-collections',
          before: ListIcon,
          text: 'List & Manage',
          to: '/collections'
        },
        {
          type: 'Item',
          id: 'new-collection',
          spacing: 'compact',
          before: AddIcon,
          text: 'Create',
          subText: 'New Collection',
          onClick: () =>
            createCollection().then(data => {
              goToThread(`/collections/edit/${data.id}`)
            })
        },
        {
          type: 'SectionHeading',
          text: 'Documents',
          spacing: 'compact',
          id: 'documents-heading'
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          component: LinkItem,
          before: ListIcon,
          id: 'list-documents',
          text: 'List & Manage',
          to: '/documents'
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          component: LinkItem,
          id: 'new-document',
          before: AddIcon,
          text: 'Create',
          subText: 'New Document(s)',
          to: '/documents/new'
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          component: LinkItem,
          id: 'list-files',
          before: VidPause,
          text: 'Pending Files',
          subText: 'Manage untyped files',
          to: '/pending-files'
        },
        {
          type: 'SectionHeading',
          spacing: 'compact',
          text: 'Document Types',
          id: 'document-types-heading'
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          before: ListIcon,
          id: 'list-document-types',
          text: 'List & Manage',
          to: '/document-types',
          component: LinkItem
        },
        {
          type: 'InlineComponent',
          spacing: 'compact',
          component: LinkItem,
          id: 'new-document-type',
          before: AddIcon,
          text: 'Create',
          subText: 'New Document Type Definition',
          to: '/document-types/new'
        }
      ]
    }
  ]
})

export default productDocumentsView
