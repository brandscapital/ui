import React from 'react'
import Avatar from '@atlaskit/avatar'
import SearchIcon from '@atlaskit/icon/glyph/search'
import AddIcon from '@atlaskit/icon/glyph/add'
import NotifIcon from '@atlaskit/icon/glyph/notification'
import SignOutIcon from '@atlaskit/icon/glyph/sign-out'
import QuestionCircleIcon from '@atlaskit/icon/glyph/question-circle'
import { GlobalNav as GlobalNavigation } from '@atlaskit/navigation-next'
import { connect } from 'react-redux'

const mapStateToProps = null

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch.auth.logout()
})

const GlobalNav = ({ logout }) => {
  const globalNavPrimaryItems = [
    { id: 'new-file', icon: AddIcon, label: 'New' },
    { id: 'search', icon: SearchIcon, label: 'Search' },
    { id: 'notifications', icon: NotifIcon, label: 'Notifications' }
  ]

  const globalNavSecondaryItems = [
    { id: 'help', icon: QuestionCircleIcon, label: 'Help', size: 'small' },
    {
      id: 'profile',
      icon: () => (
        <Avatar
          borderColor='white'
          isActive={false}
          isHover={false}
          size='small'
        />
      ),
      label: 'Profile',
      size: 'small'
    },
    {
      id: 'sign-out',
      icon: SignOutIcon,
      label: 'Sign Out',
      size: 'small',
      onClick: () => logout()
    }
  ]

  return (
    <GlobalNavigation
      primaryItems={globalNavPrimaryItems}
      secondaryItems={globalNavSecondaryItems}
    />
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(GlobalNav)
