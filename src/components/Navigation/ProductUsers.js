import React from 'react'
import { Header } from 'semantic-ui-react'
import AddIcon from '@atlaskit/icon/glyph/add-item'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productUsersView = {
  id: 'product/users',
  type: 'product',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/users:header',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/users:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      items: [
        {
          type: 'SectionHeading',
          text: 'Users',
          id: 'users-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-users',
          before: ListIcon,
          text: 'List & Manage',
          to: '/users'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-user',
          before: AddIcon,
          text: 'Create',
          subText: 'New User',
          to: '/users/new'
        }
      ]
    }
  ]
}

export default productUsersView
