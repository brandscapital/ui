import React from 'react'
import { Header } from 'semantic-ui-react'
import AddIcon from '@atlaskit/icon/glyph/add-item'
import ListIcon from '@atlaskit/icon/glyph/bullet-list'
import LinkItem from './LinkItem'

const productRecordsView = {
  id: 'product/records',
  type: 'product',
  getItems: () => [
    {
      type: 'HeaderSection',
      id: 'product/records:header',
      items: [
        {
          id: 'header',
          type: 'InlineComponent',
          component: () => (
            <div style={{ padding: '12px 0' }}>
              <Header as='h1' inverted>
                BrandsERP
              </Header>
            </div>
          )
        },
        {
          type: 'BackItem',
          id: 'back-item',
          goTo: 'product/home',
          text: 'Back'
        }
      ]
    },
    {
      type: 'MenuSection',
      nestedGroupKey: 'menu',
      id: 'product/records:menu',
      parentId: 'product/home:menu',
      alwaysShowScrollHint: true,
      items: [
        {
          type: 'SectionHeading',
          text: 'Records',
          id: 'records-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-records',
          before: ListIcon,
          text: 'List & Manage',
          to: '/records'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-record',
          before: AddIcon,
          text: 'Create',
          subText: 'New Record',
          to: '/records/new'
        },
        {
          type: 'SectionHeading',
          text: 'Record Types',
          id: 'record-types-heading'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'list-record-types',
          before: ListIcon,
          text: 'List & Manage',
          to: '/record-types'
        },
        {
          type: 'InlineComponent',
          component: LinkItem,
          id: 'new-record-type',
          before: AddIcon,
          text: 'Create',
          subText: 'New Record Type',
          to: '/record-types/new'
        }
      ]
    }
  ]
}

export default productRecordsView
