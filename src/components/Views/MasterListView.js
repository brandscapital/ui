import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Button, List, Dropdown, Grid, Segment, Header, Input, Placeholder } from 'semantic-ui-react'
import { Route, withRouter, Switch } from 'react-router-dom'
import { withNavigationViewController } from '@atlaskit/navigation-next'

import { ContainerLoader } from 'components/Loader'

class MasterListView extends Component {
  static propTypes = {
    items: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    itemRenderer: PropTypes.func,
    detailView: PropTypes.func,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    primaryActions: PropTypes.array,
    secondaryActions: PropTypes.array
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      selected: [],
      removeMode: false,
      removeTarget: null
    }
  }

  goTo = (location) => {
    this.context.router.history.push(location)
  }

  componentDidMount () {
    const { navigationViewController, navView, prefetch, match, resourceName } = this.props
    if (navView) {
      navigationViewController.setView(navView)
    }
    if (prefetch.length && match.path === `${resourceName.route}`) {
      prefetch.forEach(p => p())
    }
  }

  componentDidUpdate (prevProps) {
    const { prefetch, match } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }
  }

  triggerRemove = index => {
    this.setState({
      removeMode: true,
      removeTarget: index
    })
  }

  clearRemove = () => {
    this.setState({
      removeMode: false,
      removeTarget: null
    })
  }

  removeResource = () => {
    const { removeTarget } = this.state
    const { removeAction } = this.props
    removeAction(removeTarget).then(() => this.clearRemove())
  }

  renderConfirm = () => {
    const { removeTarget } = this.state
    const { resourceName } = this.props
    return (
      <Modal
        size='mini'
        open
        basic
        closeIcon
        onClose={() => this.clearRemove()}
      >
        <Modal.Header>
          Delete {`${resourceName.singular} - ${removeTarget}`}
        </Modal.Header>
        <Modal.Content>
          <p>
            Are you sure you want to delete this {`${resourceName.singular}`}?
          </p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            negative
            icon='checkmark'
            labelPosition='right'
            content='Delete'
            onClick={() => this.removeResource(removeTarget)}
          />
        </Modal.Actions>
      </Modal>
    )
  }

  toggleSelection = id => {
    const { selected } = this.state
    if (selected.includes(id)) {
      selected.splice(selected.indexOf(id), 1)
    } else {
      selected.push(id)
    }
    this.setState({
      selected
    })
  }

  renderItem = (v, k) => {
    const { itemRenderer, dependencies, resourceName, location, removeAction, shortcutActions } = this.props
    const { selected } = this.state
    const ItemNode = itemRenderer
    const iconProps = {
      name: selected.includes(v.id) ? 'check square' : 'square outline',
      color: selected.includes(v.id) ? 'green' : 'grey',
      size: 'large'
    }
    const itemProps = {
      dependencies,
      item: v,
      index: k
    }
    let contextMenu = null
    if (removeAction || shortcutActions) {
      contextMenu = (
        <Dropdown
          inline
          floating
          icon={{
            name: 'ellipsis horizontal',
            link: true,
            color: 'grey'
          }}
          className='icon'
        >
          <Dropdown.Menu>
            {removeAction
              ? (
                <Dropdown.Item
                  key='remove'
                  text='Delete'
                  icon={{ name: 'remove circle', color: 'red' }}
                  onClick={() => this.triggerRemove(`${v.id}`)}
                />
              ) : null
            }
            {shortcutActions ? shortcutActions(v).map((s, k) => (
              <Dropdown.Item {...s} key={k} />
            )) : null}
          </Dropdown.Menu>
        </Dropdown>
      )
    }
    return (
      <List.Item
        active={location.pathname === `${resourceName.route}/${v.id}`}
        className='list-item'
        key={k}
      >
        <Grid container columns={2} verticalAlign='middle'>
          <Grid.Column key='1' width={14} onClick={() => this.goTo(`${resourceName.route}/${v.id}`)}>
            <ItemNode {...itemProps} />
          </Grid.Column>
          <Grid.Column key='2' textAlign='right' width={2}>
            {contextMenu}
          </Grid.Column>
        </Grid>
      </List.Item>
    )
  }

  renderList = () => {
    const { items } = this.props
    return items.toList().map(this.renderItem).toJS()
  }

  render () {
    const { loading, resourceName, detailView, createView, match } = this.props
    const { removeMode } = this.state
    const Detail = detailView
    const Create = createView
    const createBtn = createView ? (
      <Button
        floated='right'
        size='tiny'
        primary
        content='New'
        icon='add circle'
        onClick={() => this.goTo(`${resourceName.route}/new`)}
      />
    ) : null
    return (
      <div className='master-list'>
        {removeMode ? this.renderConfirm() : null}
        <div className='list-wrapper'>
          <Header as='h3' attached='top'>{resourceName.plural}</Header>
          <Segment secondary size='tiny' attached compact>
            <Input icon='search' placeholder='Search...' />
            {createBtn}
          </Segment>
          <List size='small' verticalAlign='middle' divided selection>
            {loading
              ? (
                <Placeholder>
                  <Placeholder.Header image>
                    <Placeholder.Line />
                    <Placeholder.Line />
                  </Placeholder.Header>
                  <Placeholder.Line />
                  <Placeholder.Line />
                  <Placeholder.Line />
                  <Placeholder.Line />
                </Placeholder>
              )
              : this.renderList()
            }
          </List>
        </div>
        <div className='detail-wrapper'>
          {loading
            ? <ContainerLoader />
            : (
              <Switch>
                <Route exact path={`${match.url}/new`} render={
                  (props) => <Create {...props} />
                } />
                <Route exact path={`${match.url}/:id`} render={
                  (props) => {
                    return <Detail {...props} />
                  }
                } />
                <Route exact path={match.url} render={() => (
                  <h3>Select from list or create a new {`${resourceName.singular}`}</h3>
                )} />
              </Switch>
            )
          }
        </div>
      </div>
    )
  }
}

const wrapped = withRouter(withNavigationViewController(MasterListView))

export default wrapped
