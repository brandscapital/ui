import React from 'react'
import {
  ResourceList,
  Stack,
  TextStyle,
  TextContainer,
  Caption,
  Subheading
} from '@shopify/polaris'
import { Label, Icon } from 'semantic-ui-react'
import _ from 'lodash'
import moment from 'moment'

import store from 'store'

export default function ResourceListItem (props) {
  const {
    item,
    fieldList,
    url,
    removeAction,
    hideIndex,
    itemKey,
    shortcutActions,
    ...rest
  } = props

  let indexItem = null

  const fieldListCheck =
    typeof fieldList === 'function' ? fieldList(item) : fieldList
  const fields = fieldListCheck.map((field, key) => {
    const { valueRender, composite } = field
    const selector = composite
      ? _.at(item, field.properties)
      : item[field.property]
    const value = valueRender
      ? valueRender(selector, store.getState(), itemKey)
      : selector
    if (field.primary) {
      return (
        <TextStyle key={key} variation='strong'>
          {value}
        </TextStyle>
      )
    }

    if (field.index) {
      return (
        <Stack.Item key={key}>
          <Label size='large' image>
            <Icon fitted name='hashtag' />
            <Label.Detail>{itemKey}</Label.Detail>
          </Label>
        </Stack.Item>
      )
    }

    return <div key={key}>{value}</div>
  })

  const actions = [
    ...(shortcutActions ? shortcutActions(item) : []),
    { content: 'View', url, size: 'slim' }
  ]

  if (removeAction) {
    actions.push({
      content: 'Delete',
      onClick: () => removeAction(),
      destructive: true,
      size: 'slim'
    })
  }

  if (!hideIndex) {
    indexItem = (
      <Stack.Item>
        <Label size='large' image>
          <Icon fitted name='hashtag' />
          <Label.Detail>{item.id}</Label.Detail>
        </Label>
      </Stack.Item>
    )
  }

  return (
    <ResourceList.Item {...rest} shortcutActions={actions}>
      <Stack alignment='center' distribution='fillEvenly' spacing='tight'>
        <Stack.Item>
          <Stack>
            {indexItem}
            <Stack.Item fill>
              <Stack distribution='equalSpacing'>{fields}</Stack>
            </Stack.Item>
          </Stack>
        </Stack.Item>
        <Stack.Item>
          <Stack distribution='center'>
            <TextContainer spacing='tight'>
              <small>Created</small>
              <TextStyle variation='subdued'>
                <Caption>{moment(item.createdAt).format('L LT')}</Caption>
              </TextStyle>
            </TextContainer>
            <TextContainer spacing='tight'>
              <small>Updated</small>
              <TextStyle variation='subdued'>
                <Caption>{moment(item.updatedAt).format('L LT')}</Caption>
              </TextStyle>
            </TextContainer>
          </Stack>
        </Stack.Item>
      </Stack>
    </ResourceList.Item>
  )
}
