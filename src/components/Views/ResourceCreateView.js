import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Grid, Header } from 'semantic-ui-react'
import { ContainerLoader } from 'components/Loader'

import ResourceForm from './ResourceForm'

class ResourceCreateView extends Component {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  componentDidMount () {
    const { navigationViewController, navView, prefetch, resourceName, match } = this.props
    if (navView) {
      navigationViewController.setView(navView)
    }
    if (prefetch.length && match.path === `${resourceName.route}/new`) {
      prefetch.forEach(p => p())
    }
  }

  componentDidUpdate (prevProps) {
    const { prefetch, match } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }
  }

  renderContent = () => {
    const { resourceName, resourceForm, ...rest } = this.props
    const formProps = {
      resourceName,
      init: resourceForm.init,
      resourceForm,
      ...rest
    }
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Create {resourceName.singular}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <ResourceForm {...formProps} />
        </Grid.Row>
      </Grid>
    )
  }

  render () {
    const { loading, saving, resourceName } = this.props
    return loading && !saving ? (
      <ContainerLoader />
    ) : (
      this.renderContent()
    )
  }
}

export default ResourceCreateView
