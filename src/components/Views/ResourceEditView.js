import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Grid, Header } from 'semantic-ui-react'
import { withNavigationViewController } from '@atlaskit/navigation-next'
import { ContainerLoader } from 'components/Loader'

import ResourceForm from './ResourceForm'
import ResourceNotFound from './ResourceNotFound'

class ResourceEditView extends Component {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  goTo (location) {
    this.context.router.history.push(location)
  }

  componentDidMount () {
    const { navigationViewController, navView, prefetch, resourceName, match } = this.props
    if (navView) {
      navigationViewController.setView(navView)
    }
    if (prefetch.length && match.path === `${resourceName.route}/:id`) {
      prefetch.forEach(p => p())
    }
  }

  componentDidUpdate (prevProps) {
    const { prefetch, match } = this.props
    if (prefetch.length && prevProps.match.params.id !== match.params.id) {
      prefetch.forEach(p => p())
    }
  }

  renderContent = () => {
    const {
      resourceName,
      resourceForm,
      item,
      dependencies,
      ...rest
    } = this.props
    const formProps = {
      resourceName,
      resourceForm,
      init: resourceForm.beforeEdit
        ? resourceForm.beforeEdit(dependencies, item.toJS())
        : item.toJS(),
      dependencies,
      ...rest
    }
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Edit {resourceName.singular} - {item.id}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <ResourceForm {...formProps} />
        </Grid.Row>
      </Grid>
    )
  }

  render () {
    const { loading, saving, resourceName, item } = this.props
    if (loading && !saving) {
      return <ContainerLoader />
    } else {
      return item ? this.renderContent() : <ResourceNotFound />
    }
  }
}

export default ResourceEditView
