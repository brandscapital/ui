import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Divider, Segment, Grid, Button } from 'semantic-ui-react'
import { DateTimeInput } from 'semantic-ui-calendar-react'

const inputMap = {
  Input: Form.Input,
  Dropdown: Form.Dropdown,
  Radio: Form.Radio,
  DateTime: DateTimeInput
}

class ResourceForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      form: props.init
    }
  }

  static propTypes = {
    resourceName: PropTypes.object,
    resourceForm: PropTypes.object,
    init: PropTypes.object,
    dependencies: PropTypes.object,
    saving: PropTypes.bool,
    saveAction: PropTypes.func
  }

  static contextTypes = {
    router: PropTypes.object
  }

  handleSave = () => {
    const { form } = this.state
    const { saveAction, resourceName } = this.props
    saveAction(form).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  updateForm = (e, { name, value }) => {
    const { onSelect } = this.props
    const data = {}
    data[name] = value
    if (onSelect && onSelect.hasOwnProperty(name)) {
      this.setState(
        {
          form: {
            ...this.state.form,
            ...data
          }
        },
        () => onSelect[name](value)
      )
    } else {
      this.setState({
        form: {
          ...this.state.form,
          ...data
        }
      })
    }
  }

  render () {
    const { form } = this.state
    const {
      resourceForm,
      resourceName,
      dependencies,
      saving
    } = this.props
    const { fields, validations } = resourceForm
    const groups = fields.map((group, k) => {
      const inputs = group.map((v, k) => {
        if (v.component === 'Dropdown') {
          const options = v.getOptions(dependencies, form)
          v.props.options = options
        }
        if (v.component === 'Radio') {
          v.props.checked = form[v.name] === v.props.value
        }
        if (v.props.depends) {
          v.props.disabled = !form[v.props.depends]
        }
        const value =
          v.getValue && !v.props.disabled
            ? v.getValue(dependencies, form)
            : form[v.name]
        const Field = inputMap[v.component]
        return (
          <Field
            key={k}
            {...v.props}
            name={v.name}
            value={value || ''}
            onChange={this.updateForm}
          />
        )
      })
      return (
        <Form.Group key={k} widths='equal'>
          {inputs}
        </Form.Group>
      )
    })
    const isValid = validations.reduce(
      (r, v, k) => (r = v.rule(form, form[v.name])),
      false
    )
    return (
      <Grid.Column>
        <Segment padded loading={saving}>
          <Form onSubmit={this.handleSave}>
            {groups}
          </Form>
          <Divider clearing hidden />
        </Segment>
        <Button
          floated='right'
          positive
          content={`Save ${resourceName.singular}`}
          onClick={() => this.handleSave()}
          disabled={!isValid}
        />
      </Grid.Column>
    )
  }
}

export default ResourceForm
