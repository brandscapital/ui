import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Page, Layout, ResourceList, TextStyle, Card } from '@shopify/polaris'
import { Modal, Button, Segment } from 'semantic-ui-react'

import { withNavigationViewController } from '@atlaskit/navigation-next'

import { PageSkeleton } from 'components/Skeletons'
import ResourceListItem from './ResourceListItem'
import ResourceListEmpty from './ResourceListEmpty'

class ResourceListView extends Component {
  static propTypes = {
    items: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceFields: PropTypes.shape({
      property: PropTypes.string,
      label: PropTypes.string
    }),
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    disableAction: PropTypes.bool,
    hideIndex: PropTypes.bool,
    removeAction: PropTypes.func,
    viewAction: PropTypes.func
  }

  state = {
    removeMode: false,
    removeTarget: null
  }

  componentDidMount () {
    const { navigationViewController, navView } = this.props
    if (navView) {
      navigationViewController.setView(navView)
    }
  }

  componentWillMount () {
    const { prefetch } = this.props
    if (prefetch.length) {
      prefetch.forEach(p => p())
    }
  }

  triggerRemove = index => {
    this.setState({
      removeMode: true,
      removeTarget: index
    })
  }

  clearRemove = () => {
    this.setState({
      removeMode: false,
      removeTarget: null
    })
  }

  removeResource = () => {
    const { removeTarget } = this.state
    const { removeAction } = this.props
    removeAction(removeTarget).then(() => this.clearRemove())
  }

  renderConfirm = () => {
    const { removeTarget } = this.state
    const { items, resourceName } = this.props
    return (
      <Modal
        size='mini'
        open
        basic
        closeIcon
        onClose={() => this.clearRemove()}
      >
        <Modal.Header>
          Delete {`${resourceName.singular} - ${removeTarget}`}
        </Modal.Header>
        <Modal.Content>
          <p>
            Are you sure you want to delete this {`${resourceName.singular}`}?
          </p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            negative
            icon='checkmark'
            labelPosition='right'
            content='Delete'
            onClick={() => this.removeResource(removeTarget)}
          />
        </Modal.Actions>
      </Modal>
    )
  }

  itemRenderer = (item, id) => {
    const {
      resourceName,
      resourceList,
      removeAction,
      shortcutActions,
      ...rest
    } = this.props
    const itemProps = {
      resourceName,
      fieldList: resourceList.fields,
      item,
      url: `${resourceName.route}/edit/${item.id}`,
      shortcutActions,
      removeAction: removeAction ? () => this.triggerRemove(item.id) : null,
      itemKey: id,
      ...rest
    }
    return <ResourceListItem key={id} {...itemProps} />
  }

  renderContent = () => {
    const {
      resourceName,
      resourceList,
      items,
      primaryAction,
      disableAction
    } = this.props
    const { removeMode } = this.state
    const resourceListProps = {
      items: items.toList().toJS(),
      resourceName,
      renderItem: this.itemRenderer
    }
    if (resourceList.idForItem) {
      resourceListProps.idForItem = resourceList.idForItem
    }
    const pageProps = {
      primaryAction: !disableAction
        ? [
          {
            content: `Create ${resourceName.singular}`,
            url: `${resourceName.route}/new`
          }
        ]
        : null,
      fullWidth: true,
      title: resourceName.plural
    }
    if (primaryAction) {
      pageProps.primaryAction = primaryAction
    }
    return (
      <Page {...pageProps}>
        {removeMode ? this.renderConfirm() : null}
        <Layout>
          <Layout.Section>
            {items.count() ? (
              <Card>
                <ResourceList {...resourceListProps} />
              </Card>
            ) : (
              <ResourceListEmpty name={resourceName} />
            )}
          </Layout.Section>
        </Layout>
      </Page>
    )
  }

  render () {
    const { loading, resourceName } = this.props
    return loading ? (
      <PageSkeleton title={resourceName.plural} />
    ) : (
      this.renderContent()
    )
  }
}

export default ResourceListView
