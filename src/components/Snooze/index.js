import React, { Component } from 'react'
import propTypes from 'prop-types'
import { Popup, Header, Input, Label, Icon } from 'semantic-ui-react'

export default class Snooze extends Component {
  static propTypes = {
    onSubmit: propTypes.func.isRequired,
    taskTypeId: propTypes.number.isRequired,
    collectionId: propTypes.number.isRequired
  }

  state = {
    days: 7,
    isOpen: false
  }

  onChange = (e, { value }) => {
    this.setState({
      days: value
    })
  }

  open = () => {
    this.setState({
      isOpen: true
    })
  }

  close = () => {
    this.setState({
      isOpen: false
    })
  }

  handleSave = () => {
    const { onSubmit, taskTypeId, collectionId } = this.props
    const { days } = this.state
    onSubmit({
      minutes: 1440 * days,
      task_type_id: taskTypeId,
      collection_id: collectionId
    }).then(() => this.close())
  }

  render () {
    const { days, isOpen } = this.state
    const snoozeNode = (
      <Label as='a'>
        <Icon name='clock' /> Snooze
      </Label>
    )
    return (
      <Popup
        trigger={snoozeNode}
        on='click'
        open={isOpen}
        onOpen={this.open}
        onClose={this.close}
        position='bottom right'
      >
        <Header size='tiny' content='Set Days To Remind' />
        <Input
          size='small'
          type='number'
          action={{
            color: 'teal',
            content: 'Create',
            size: 'small',
            disabled: days < 1,
            onClick: () => this.handleSave()
          }}
          placeholder='Days'
          value={days}
          onChange={this.onChange}
        />
      </Popup>
    )
  }
}
