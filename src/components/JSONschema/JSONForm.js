import React from 'react'
import { Form as FormSematicUI, Button } from 'semantic-ui-react'
import Form from 'react-jsonschema-form-semanticui-fixed'

export default class JSONForm extends Form {
  render () {
    const {
      children,
      safeRenderCompletion,
      id,
      name,
      method,
      target,
      action,
      autocomplete,
      enctype,
      acceptcharset,
      noHtml5Validate,
      className
    } = this.props

    const { schema, uiSchema, formData, errorSchema, idSchema } = this.state
    const registry = this.getRegistry()
    const _SchemaField = registry.fields.SchemaField
    return (
      <FormSematicUI
        id={id}
        name={name}
        method={method}
        target={target}
        action={action}
        autoComplete={autocomplete}
        encType={enctype}
        acceptCharset={acceptcharset}
        noValidate={noHtml5Validate}
        onSubmit={this.onSubmit}
        className={className}
      >
        {this.renderErrors()}
        <_SchemaField
          schema={schema}
          uiSchema={uiSchema}
          errorSchema={errorSchema}
          idSchema={idSchema}
          formData={formData}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          registry={registry}
          safeRenderCompletion={safeRenderCompletion}
        />
        {children || (
          <Button size='small' primary type='submit'>
            Submit
          </Button>
        )}
      </FormSematicUI>
    )
  }
}
