import React from 'react'
import {
  SkeletonPage,
  Layout,
  Card,
  TextContainer,
  SkeletonDisplayText,
  SkeletonBodyText
} from '@shopify/polaris'

export default ({ title, breadcrumbs }) => (
  <SkeletonPage title={title} breadcrumbs={breadcrumbs} fullWidth>
    <Layout>
      <Layout.Section>
        <Card sectioned>
          <TextContainer>
            <SkeletonDisplayText />
            <SkeletonBodyText />
          </TextContainer>
        </Card>
      </Layout.Section>
    </Layout>
  </SkeletonPage>
)
