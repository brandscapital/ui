import { Record, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'
import moment from 'moment'
import momentJdate from 'moment-jdateformatparser'

import { ucFirst } from 'utils/render'
import store from 'store'
import { extendFactory } from 'models/base'
import api from 'middleware/camunda'

const Task = new Record({
  id: null,
  name: null,
  assignee: null,
  due: null,
  owner: null,
  delegationState: null,
  taskDefinitionKey: null,
  description: null,
  created: null,
  followUp: null,
  priority: null,
  parentTaskId: null,
  caseInstanceId: null,
  tenantId: null,
  formVariables: fromJS({})
})

export const modelConfig = {
  resourceName: {
    singular: 'Task',
    plural: 'Tasks',
    route: '/tasks'
  },
  resourceList: state => ({
    idForItem: (item, index) => index,
    fields: [
      {
        property: 'id',
        label: 'id',
        index: true
      },
      { property: 'name', label: 'Name', primary: true },
      {
        property: 'assignee',
        label: 'Assignee',
        valueRender: (v, state) => {
          let user = state.users.getIn(['list', `${v}`])
          return user ? `${user.firstName} ${user.lastName}` : 'Unassigned'
        }
      },
      {
        property: 'delegationState',
        label: 'Status',
        valueRender: v => v || 'Incomplete'
      }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      description: '',
      assignee: '',
      delegationState: '',
      due: '',
      priority: ''
    },
    beforeSave: (deps, form) => {
      form.due = form.due
        ? moment(form.due)
          .formatWithJDF("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
          .toString()
        : null
      form.followUp = form.followUp
        ? moment(form.followUp)
          .formatWithJDF("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
          .toString()
        : null
      return form
    },
    beforeEdit: (deps, form) => {
      form.due = form.due ? moment(form.due).format('L LT') : null
      form.followUp = form.followUp
        ? moment(form.followUp).format('L LT')
        : null
      return form
    },
    validations: [{ name: 'name', rule: (form, v) => v !== '' }],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description'
          }
        }
      ],
      [
        {
          name: 'assignee',
          component: 'Dropdown',
          getOptions: deps =>
            deps.users.map(r => ({
              key: r.id,
              text: `${r.firstName} ${r.lastName}`,
              value: `${r.id}`
            })),
          props: {
            placeholder: 'Choose assignee',
            label: 'Assignee',
            search: true,
            selection: true,
            clearable: true
          }
        },
        {
          name: 'priority',
          component: 'Input',
          props: {
            type: 'number',
            placeholder: 'Priority',
            label: 'Priority',
            required: true
          }
        }
      ]
    ]
  },
  apiPath: 'task',
  schemaName: 'tasks',
  recordType: Task
}

const modelEffects = config => ({
  async fetch ({ id, params }) {
    const { modelSchema } = config
    const res = await api.get(`task/${id}`, { params })
    this.push(normalize(res.data, modelSchema))
  },
  async fetchAll (params) {
    const { listSchema } = config
    const res = await api.get(`task`, { params })
    this.merge(normalize(res.data, listSchema))
  },
  create (item) {
    const { apiPath, resourceName, modelSchema } = config
    return api
      .post(`${apiPath}/create`, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Created Successfully`)
        this.push(normalize(data, modelSchema))
        return data
      })
      .catch(e => console.error(e))
  },
  update (item) {
    const { apiPath, resourceName, modelSchema } = config
    return api
      .put(`${apiPath}/${item.id}`, item)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Updated Successfully`)
        this.push(normalize(item, modelSchema))
        return item
      })
      .catch(e => console.error(e))
  },
  claimTask (taskId, rootState) {
    const body = { userId: rootState.auth.user.id }
    return api
      .post(`/task/${taskId}/claim`, JSON.stringify(body))
      .then(({ data }) => {
        toastr.success('Task Claimed Successfully')
        this.claim({ userId: body.userId, taskId })
        return data
      })
      .catch(e => console.error(e))
  }
})

const modelReducers = config => ({
  claim: (state, payload) => {
    const { userId, taskId } = payload
    let task = state.getIn(['list', `${taskId}`])
    task = task.set('assignee', userId)
    return state.setIn(['list', `${taskId}`], task)
  }
})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
