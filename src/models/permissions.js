import { Record } from 'immutable'
import { schema } from 'normalizr'

import { extendFactory } from 'models/base'

export const permissionSchema = new schema.Entity('permissions')

const Permission = new Record({
  id: null,
  name: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Permission',
    plural: 'Permissions',
    route: '/permissions'
  },
  resourceList: state => ({
    fields: [{ property: 'name', label: 'Name', primary: true }]
  }),
  resourceForm: {
    init: {
      name: ''
    },
    validations: [{ name: 'name', rule: (form, v) => v !== '' }],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        }
      ]
    ]
  },
  schemaName: 'permissions',
  recordType: Permission,
  modelSchema: permissionSchema
}
const model = extendFactory(modelConfig)

export default model
