import { Record, fromJS } from 'immutable'
import { schema } from 'normalizr'

import { extendFactory } from 'models/base'
import { permissionSchema } from './permissions'

export const roleSchema = new schema.Entity('roles', {
  permissions: [permissionSchema]
})

const Role = new Record({
  id: null,
  name: null,
  permissions: fromJS([]),
  guard: 'api',
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Role',
    plural: 'Roles',
    route: '/roles'
  },
  resourceList: state => ({
    fields: [{ property: 'name', label: 'Name', primary: true }]
  }),
  resourceForm: {
    init: {
      name: '',
      permissions: []
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'permissions', rule: (form, v) => v.length > 0 }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'permissions',
          component: 'Dropdown',
          getOptions: (deps) =>
            deps.permissions.toList().map(r => ({
              key: r.id,
              text: r.name,
              value: r.id
            })).toJS(),
          props: {
            placeholder: 'Choose permissions',
            label: 'Permissions',
            required: true,
            search: true,
            selection: true,
            multiple: true
          }
        }
      ]
    ]
  },
  schemaName: 'roles',
  recordType: Role,
  modelSchema: roleSchema
}
const model = extendFactory(modelConfig)

export default model
