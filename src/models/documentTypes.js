import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'

import { extendFactory } from 'models/base'
import api from 'middleware/api'
import store from 'store'

export const DocumentTypeSchema = new schema.Entity('documentTypes')

export const DocumentTypeListSchema = new schema.Array(DocumentTypeSchema)

const DocumentType = new Record({
  id: null,
  name: null,
  description: null,
  properties: {
    fileTypes: []
  },
  media: [],
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Document Type',
    plural: 'Document Types',
    route: '/document-types'
  },
  resourceList: state => ({
    fields: [
      { property: 'name', label: 'Name', primary: true },
      { property: 'description', label: 'Description' }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      properties: {
        fileTypes: []
      },
      description: ''
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'description', rule: (form, v) => v !== '' }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description',
            required: true
          }
        }
      ],
      [
        {
          name: 'properties',
          component: 'Dropdown',
          getOptions: (deps, form) =>
            deps.fileTypes[form.fileType].map((r, k) => ({
              key: k,
              text: r.name,
              value: k
            })),
          props: {
            search: true,
            selection: true,
            required: true,
            placeholder: 'Choose file type',
            label: 'File Type'
          }
        }
      ]
    ]
  },
  schemaName: 'documentTypes',
  modelSchema: DocumentTypeSchema,
  listSchema: DocumentTypeListSchema,
  recordType: DocumentType,
  apiPath: 'document_types'
}

const modelEffects = config => ({
  async fetchWithMedia (id = null) {
    const { listSchema } = config
    const url = id === null ? '/documents' : `/documents/${id}`
    const res = await api.get(url, { with: 'media' })
    const normalized = normalize(res.data, listSchema)
    this.push(normalized)
    store.dispatch.documents.push(normalized)
    return res.data
  }
})

const modelReducers = config => ({})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
