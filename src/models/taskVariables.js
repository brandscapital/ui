import { Record, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import { extendFactory } from 'models/base'
import api from 'middleware/camunda'

export const TaskVariables = new Record({
  id: null,
  formVariables: fromJS({})
})

const modelConfig = {
  resourceName: {
    singular: 'TaskVariable',
    plural: 'TaskVariables'
  },
  schemaName: 'taskVariables',
  recordType: TaskVariables
}

const modelEffects = config => ({
  async fetch (id) {
    const { modelSchema } = config
    const res = await api.get(`/task/${id}/form-variables`)
    this.push(normalize({ formVariables: res.data, id }, modelSchema))
    return res.data
  }
})

const modelReducers = config => ({})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
