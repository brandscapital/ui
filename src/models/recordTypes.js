import { Record } from 'immutable'

import { extendFactory } from 'models/base'

const RecordType = new Record({
  id: null,
  name: null,
  description: null,
  meta: {
    listFields: []
  },
  schema: null,
  uiSchema: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Record Type',
    plural: 'Record Types',
    route: '/record-types'
  },
  resourceList: state => ({
    fields: [
      { property: 'name', label: 'Name', primary: true },
      { property: 'description', label: 'Description' }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      description: ''
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'description', rule: (form, v) => v !== '' }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description',
            required: true
          }
        }
      ]
    ]
  },
  schemaName: 'recordTypes',
  apiPath: 'record_types',
  recordType: RecordType
}

const model = extendFactory(modelConfig)

export default model
