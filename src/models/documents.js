import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import api from 'middleware/api'
import store from 'store'
import { extendFactory } from 'models/base'
import { DocumentTypeSchema } from './documentTypes'
import { processSchema } from './processes'

export const DocumentSchema = new schema.Entity('documents', {
  documentType: DocumentTypeSchema,
  processes: [processSchema]
})

const Document = new Record({
  id: null,
  filename: null,
  extension: null,
  mimeType: null,
  documentTypeId: null,
  documentType: null,
  processes: [],
  url: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Document',
    plural: 'Documents',
    route: '/documents'
  },
  resourceList: state => ({
    fields: [
      {
        property: 'documentTypeId',
        label: 'Document Type',
        primary: true,
        valueRender: v =>
          state.documentTypes.getIn(['list', `${v.docoumentTypeId}`, 'name'])
      },
      {
        properties: ['filename', 'extension'],
        label: 'Filename',
        composite: true,
        valueRender: v => `${v[0]}.${v[1]}`
      },
      { property: 'mimeType', label: 'MIME Type' }
    ]
  }),
  resourceForm: {
    init: {
      name: '',
      properties: {
        fileTypes: []
      },
      description: ''
    },
    validations: [
      { name: 'name', rule: (form, v) => v !== '' },
      { name: 'description', rule: (form, v) => v !== '' }
    ],
    fields: [
      [
        {
          name: 'name',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Name',
            label: 'Name',
            required: true
          }
        },
        {
          name: 'description',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Description',
            label: 'Description',
            required: true
          }
        }
      ],
      [
        {
          name: 'properties',
          component: 'Dropdown',
          getOptions: (deps, form) =>
            deps.fileTypes[form.fileType].map((r, k) => ({
              key: k,
              text: r.name,
              value: k
            })),
          props: {
            search: true,
            selection: true,
            required: true,
            placeholder: 'Choose file type',
            label: 'File Type'
          }
        }
      ]
    ]
  },
  schemaName: 'documents',
  modelSchema: DocumentSchema,
  recordType: Document,
  apiPath: 'documents'
}

const modelReducers = config => ({})

const modelEffects = config => ({
  async fetchPending (id = null) {
    const { listSchema } = config
    const url =
      id === null ? '/documents/pending' : `/documents/${id}?pending=true`
    const res = await api.get(url)
    const normalized = normalize(res.data, listSchema)
    this.push(normalized)
    return res.data
  },
  async fetchAll () {
    const { listSchema, apiPath } = config
    const res = await api.get(apiPath)
    const normalized = normalize(res.data, listSchema)
    this.merge(normalized)
    store.dispatch.documentTypes.merge(normalized)
    return res.data
  },
  async fetch ({ id, params }) {
    const { modelSchema, apiPath } = config
    const res = await api.get(`${apiPath}/${id}`, { params })
    const normalized = normalize(res.data, modelSchema)
    this.push(normalized)
    store.dispatch.documentTypes.push(normalized)
    store.dispatch.processes.push(normalized)
    return res.data
  },
  removePending (id) {
    const { apiPath, resourceName } = config
    return api
      .delete(`${apiPath}/${id}?pending=true`)
      .then(({ data }) => {
        toastr.success(`${resourceName.singular} Deleted Successfully`)
        this.delete(id)
        return data
      })
      .catch(e => console.error(e))
  }
})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
