import { Record } from 'immutable'
import { normalize, schema } from 'normalizr'

import { extendFactory } from 'models/base'
import api from 'middleware/api'
import { DocumentSchema } from './documents'
import store from 'store'

export const collectionSchema = new schema.Entity('collections', {
  media: [DocumentSchema]
})

export const Collection = new Record({
  id: null,
  name: null,
  media: [],
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Collection',
    plural: 'Collections',
    route: '/collections'
  },
  resourceList: state => ({
    fields: [
      {
        property: 'name',
        label: 'Name',
        primary: true,
        valueRender: v => v || 'Untitled'
      }
    ]
  }),
  resourceForm: {},
  schemaName: 'collections',
  recordType: Collection,
  modelSchema: collectionSchema
}

const modelEffects = config => ({
  async fetch ({ id, params }) {
    const { modelSchema, apiPath } = config
    const res = await api.get(`${apiPath}/${id}`, { params })
    const normalized = normalize(res.data, modelSchema)
    this.push(normalized)
    store.dispatch.documents.push(normalized)
    store.dispatch.documentTypes.push(normalized)
    return res.data
  },
  attach ({ id, docs }) {
    const { modelSchema } = config
    return api
      .post(`/${config.apiPath}/attach/${id}`, docs)
      .then(({ data }) => {
        const normalized = normalize(data, modelSchema)
        this.push(normalized)
        store.dispatch.documents.push(normalized)
        return data
      })
  },
  detach ({ id, docs }) {
    const { modelSchema } = config
    return api
      .post(`/${config.apiPath}/detach/${id}`, docs)
      .then(({ data }) => {
        const normalized = normalize(data, modelSchema)
        this.push(normalized)
        return data
      })
  }
})

const modelReducers = config => ({})

const model = extendFactory(modelConfig, modelReducers, modelEffects)

export default model
