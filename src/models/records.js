import { Record as IRecord } from 'immutable'

import { extendFactory } from 'models/base'

const Record = new IRecord({
  id: null,
  recordTypeId: null,
  data: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'Record',
    plural: 'Records',
    route: '/records'
  },
  resourceList: state => ({
    fields: item => {
      let rt = state.recordTypes.getIn(['list', `${item.recordTypeId}`]).toJS()
      const { listFields } = rt.meta
      const customFields = listFields.map((v, k) => {
        let prop = rt.schema.properties[v]
        return {
          property: v,
          label: prop.title,
          valueRender: r => item.data[v]
        }
      })
      return [
        {
          property: 'recordTypeId',
          label: 'Record Type',
          valueRender: v => state.recordTypes.getIn(['list', `${v}`, 'name']),
          primary: true
        },
        ...customFields
      ]
    }
  }),
  resourceForm: {
    init: {
      recordTypeId: '',
      data: ''
    },
    validations: [{ name: 'recordTypeId', rule: (form, v) => v !== '' }],
    fields: [
      [
        {
          name: 'recordTypeId',
          component: 'Dropdown',
          getOptions: deps =>
            deps.recordTypes.map(r => ({
              key: r.id,
              text: r.name,
              value: r.id
            })),
          props: {
            search: true,
            selection: true,
            placeholder: 'Choose a record type',
            label: 'Record Type',
            clearable: true
          }
        },
        {
          name: 'data',
          component: 'JSONSchema',
          props: {}
        }
      ]
    ]
  },
  schemaName: 'records',
  recordType: Record
}
const model = extendFactory(modelConfig)

export default model
