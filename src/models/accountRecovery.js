import { Record } from 'immutable'

import api from 'middleware/api'

const initialState = new Record({
  didReset: false,
  isReseting: false,
  message: null
})

export default {
  state: new initialState(),
  reducers: {
    requestSending: state => state.set('isReseting', true),
    requestFailed: (state, payload) => {
      return state
        .set('message', payload)
        .set('didReset', false)
        .set('isReseting', false)
    },
    requestSuccess: (state, payload) => {
      return state
        .set('message', payload)
        .set('didReset', true)
        .set('isReseting', false)
    }
  }
}
