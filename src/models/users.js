import { Record } from 'immutable'
import { schema } from 'normalizr'
import _ from 'lodash'

import { extendFactory } from 'models/base'
import { roleSchema } from './roles'

export const userSchema = new schema.Entity('users', {
  roles: [roleSchema]
})

const User = new Record({
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  roles: null,
  createdAt: null,
  updatedAt: null
})

export const modelConfig = {
  resourceName: {
    singular: 'User',
    plural: 'Users',
    route: '/users'
  },
  resourceList: state => ({
    fields: [
      {
        properties: ['firstName', 'lastName'],
        label: 'Name',
        composite: true,
        valueRender: v => `${v[0]} ${v[1]}`,
        primary: true
      },
      { property: 'email', label: 'E-mail' }
    ]
  }),
  resourceForm: {
    init: {
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
      email: '',
      roles: []
    },
    beforeSave: (deps, form) => {
      return _.omit(form, ['confirmPassword'])
    },
    validations: [
      { name: 'firstName', rule: (form, v) => v !== '' },
      { name: 'lastName', rule: (form, v) => v !== '' },
      { name: 'email', rule: (form, v) => v !== '' },
      { name: 'password', rule: (form, v) => v !== '' },
      {
        name: 'confirmPassword',
        rule: (form, v) => v !== '' && v === form['confirmPassword']
      }
    ],
    fields: [
      [
        {
          name: 'firstName',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'First Name',
            label: 'First Name',
            required: true
          }
        },
        {
          name: 'lastName',
          component: 'Input',
          props: {
            type: 'text',
            placeholder: 'Last Name',
            label: 'Last Name',
            required: true
          }
        }
      ],
      [
        {
          name: 'email',
          component: 'Input',
          props: {
            type: 'email',
            placeholder: 'E-mail',
            label: 'E-mail',
            required: true
          }
        },
        {
          name: 'roles',
          component: 'Dropdown',
          getOptions: deps =>
            deps.roles.map(r => ({
              key: r.id,
              text: r.name,
              value: r.id
            })),
          props: {
            search: true,
            selection: true,
            multiple: true,
            placeholder: 'Choose a role',
            label: 'Role'
          }
        }
      ],
      [
        {
          name: 'password',
          component: 'Input',
          props: {
            type: 'password',
            placeholder: 'Password',
            label: 'Password',
            required: true
          }
        },
        {
          name: 'confirmPassword',
          component: 'Input',
          props: {
            type: 'password',
            placeholder: 'Confirm Password',
            label: 'Confirm Password',
            required: true
          }
        }
      ]
    ]
  },
  schemaName: 'users',
  recordType: User,
  modelSchema: userSchema
}

const model = extendFactory(modelConfig)

export default model
