import { OrderedMap, fromJS } from 'immutable'
import { normalize, schema } from 'normalizr'
import { toastr } from 'react-redux-toastr'

import api from 'middleware/api'
import { ucFirst } from 'utils/render'

export const extendFactory = (
  config = {},
  reducers = c => ({}),
  effects = c => ({}),
  selectors = {}
) => {
  const baseConfig = {
    resourceName: {
      singular: 'Base',
      plural: 'Bases'
    },
    resourceListFields: state => [
      { property: 'id', label: 'ID' },
      { property: 'createdAt', label: 'Created At' },
      { property: 'updatedAt', label: 'Updated At' }
    ],
    schemaName: 'basemodel',
    recordType: null,
    modelSchema: null,
    listSchema: null,
    apiPath: null
  }

  let _config = { ...baseConfig, ...config }
  if (!_config.modelSchema) {
    _config.modelSchema = new schema.Entity(`${_config.schemaName}`)
  }
  if (!_config.listSchema) {
    _config.listSchema = new schema.Array(_config.modelSchema)
  }
  if (!_config.apiPath) {
    _config.apiPath = `${_config.schemaName}`
  }

  const baseReducers = {
    delete: (state, id) => state.deleteIn(['list', `${id}`]),
    merge: (state, { entities, result }) => {
      const { recordType, schemaName } = _config
      if (result && result.length) {
        const items = fromJS(entities[schemaName]).map(t => new recordType(t))
        return state.mergeDeepIn(['list'], items)
      }
      return state
    },
    push: (state, { entities, result }) => {
      const { recordType, schemaName } = _config
      if (result && entities[schemaName]) {
        const item = fromJS(entities[schemaName]).map(t => new recordType(t))
        return state.mergeIn(['list'], item)
      }
      return state
    }
  }

  const baseEffects = {
    remove (id) {
      const { apiPath, resourceName } = _config
      return api
        .delete(`${apiPath}/${id}`)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Deleted Successfully`)
          this.delete(id)
          return data
        })
        .catch(e => console.error(e))
    },
    update (item) {
      const { apiPath, resourceName, modelSchema } = _config
      return api
        .post(`${apiPath}/${item.id}`, item)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Updated Successfully`)
          this.push(normalize(data, modelSchema))
          return data
        })
        .catch(e => console.error(e))
    },
    create (item) {
      const { apiPath, resourceName, modelSchema } = _config
      return api
        .post(apiPath, item)
        .then(({ data }) => {
          toastr.success(`${resourceName.singular} Created Successfully`)
          this.push(normalize(data, modelSchema))
          return data
        })
        .catch(e => console.error(e))
    },
    async fetchAll () {
      const { listSchema, apiPath } = _config
      const res = await api.get(apiPath)
      this.merge(normalize(res.data, listSchema))
      return res.data
    },
    async fetch ({ id, params }) {
      const { modelSchema, apiPath } = _config
      const res = await api.get(`${apiPath}/${id}`, { params })
      this.push(normalize(res.data, modelSchema))
      return res.data
    }
  }

  return {
    state: fromJS({
      list: new OrderedMap()
    }),
    reducers: { ...baseReducers, ...reducers(_config) },
    effects: { ...baseEffects, ...effects(_config) },
    selectors: { ...selectors }
  }
}
