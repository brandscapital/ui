export const commonSortOptions = [
  { label: 'Newest First', orderBy: 'createdAt', sortedBy: 'desc' }
  { label: 'Oldest First', orderBy: 'createdAt', sortedBy: 'asc' }
  { label: 'Most Recent Update', orderBy: 'updatedAt', sortedBy: 'desc' },
  { label: 'Least Recent Update', orderBy: 'updatedAt', sortedBy: 'asc' }
]
