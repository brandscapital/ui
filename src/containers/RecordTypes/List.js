import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/recordTypes'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RecordTypesListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.recordTypes.get('list'),
    resourceName,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'product/records',
    loading: state.loading.effects.recordTypes.fetchAll || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.recordTypes.fetchAll()],
    refetch: ops => dispatch.recordTypes.fetchAll(ops),
    removeAction: id => dispatch.recordTypes.remove(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
