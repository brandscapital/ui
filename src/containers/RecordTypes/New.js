import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import RecordTypeCreateView from './components/RecordTypeCreateView'
import { modelConfig } from 'models/recordTypes'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    resourceName,
    resourceForm,
    navView: 'product/records',
    saving: state.loading.effects.recordTypes.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [],
    saveAction: recordType => dispatch.recordTypes.create(recordType)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(RecordTypeCreateView)
)
