import React from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Header, Segment, Grid, Label, Menu } from 'semantic-ui-react'
import _ from 'lodash'

import { ResourceEditView } from 'components/Views'
import { toJson, shouldRender } from 'utils/render'
import JSONForm from 'components/JSONschema/JSONForm'
import Editor from 'components/Editor'

class RecordTypeEditView extends ResourceEditView {
  constructor (props) {
    super(props)
    this.state = {
      toggleView: 'json',
      form: {
        id: props.item ? props.item.id : '',
        name: props.item ? props.item.name : '',
        description: props.item ? props.item.description : ''
      },
      schema: props.item ? props.item.schema.toJS() : {},
      uiSchema: props.item ? props.item.uiSchema.toJS() : {},
      formData: {},
      meta: {
        listFields: []
      }
    }
  }

  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount () {
    const { navigationViewController } = this.props
    navigationViewController.setView('product/records')
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      this.setState({
        form: {
          id: nextProps.item.id,
          name: nextProps.item.name,
          description: nextProps.item.description
        },
        schema: nextProps.item.schema.toJS(),
        uiSchema: nextProps.item.uiSchema.toJS(),
        meta: nextProps.item.meta.toJS()
      })
    }
  }

  componentWillMount () {
    const { prefetch } = this.props
    if (prefetch && prefetch.length) {
      prefetch.forEach(p => p())
    }
  }

  updateSchema = schema => {
    this.setState({
      schema
    })
  }

  updateUiSchema = uiSchema => {
    this.setState({
      uiSchema
    })
  }

  updateMeta = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      meta: {
        ...this.state.meta,
        ...data
      }
    })
  }

  updateFormData = ({ formData }) => {
    this.setState({
      formData
    })
  }

  updateView = (e, { name }) => this.setState({ toggleView: name })

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form, schema, uiSchema, meta } = this.state
    const newRecordType = {
      ...form,
      schema,
      uiSchema,
      meta
    }
    saveAction(newRecordType).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderPane = () => {
    const { toggleView } = this.state
    return toggleView === 'options'
      ? this.renderOptions()
      : this.renderSchemaBuilder()
  }

  renderOptions = () => {
    const { schema, meta } = this.state
    const listFieldOps = _.keys(schema.properties).map((v, k) => ({
      key: k,
      text: v,
      value: v
    }))
    return (
      <Form size='small'>
        <Form.Dropdown
          multiple
          selection
          options={listFieldOps}
          name='listFields'
          label='List Display Fields'
          value={meta.listFields}
          onChange={this.updateMeta}
        />
      </Form>
    )
  }

  renderSchemaBuilder = () => {
    const { schema, formData, uiSchema, toggleView } = this.state

    const viewerProps = {
      formData,
      onChange: this.updateFormData,
      schema,
      uiSchema,
      className: 'ui tiny form'
    }

    const editorProps = {
      onChange: toggleView === 'ui' ? this.updateUiSchema : this.updateSchema,
      code: toggleView === 'ui' ? toJson(uiSchema) : toJson(schema),
      label: toggleView
    }

    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column width={9}>
            <Editor {...editorProps} />
          </Grid.Column>
          <Grid.Column width={7}>
            <Segment>
              <Label attached='top left' content='Form Preview' />
              <JSONForm {...viewerProps} />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  renderContent = () => {
    const { uiState, schema, toggleView } = this.state
    const { name, description } = this.state.form
    const { resourceName, resourceForm, saving, item } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = name !== ''
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Edit Record Type - {item.id}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <Form size='small' onSubmit={this.handleSave}>
                <Form.Group widths='equal'>
                  <Form.Input
                    label='Name'
                    type='text'
                    name='name'
                    value={name}
                    required
                    onChange={this.updateForm}
                  />
                  <Form.Input
                    label='Description'
                    type='text'
                    name='description'
                    value={description}
                    onChange={this.updateForm}
                  />
                </Form.Group>
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Menu size='tiny' attached='top'>
              <Menu.Item
                name='json'
                content='JSON Schema'
                active={toggleView === 'json'}
                onClick={this.updateView}
              />
              <Menu.Item
                name='ui'
                content='UI Schema'
                active={toggleView === 'ui'}
                onClick={this.updateView}
              />
              <Menu.Item
                name='options'
                content='Options'
                active={toggleView === 'options'}
                onClick={this.updateView}
              />
            </Menu>
            <Segment attached='bottom'>{this.renderPane()}</Segment>
            <Button floated='right' disabled={!isValid} positive content='Save' onClick={() => this.handleSave()} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default RecordTypeEditView
