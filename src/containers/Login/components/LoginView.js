import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Message, Divider, Button } from 'semantic-ui-react'

class LoginView extends Component {
  state = {
    form: {
      email: '',
      password: ''
    }
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  handleLogin = e => {
    e.preventDefault()
    const { email, password } = this.state.form
    if (email === '' || password === '') {
      return false
    }
    this.props.doLogin({ email, password })
  }

  render () {
    const { auth } = this.props
    const hasError = auth.loginError !== null
    return (
      <div className='ui six wide column'>
        <div className='ui very padded segment'>
          <h1 className='ui teal image header'>
            <div className='content'>Brands ERP</div>
          </h1>
          <Form onSubmit={this.handleLogin} error={hasError}>
            <Message
              size='tiny'
              error
              header='Login Failure'
              content={auth.loginError}
            />
            <Form.Input
              placeholder='E-mail'
              name='email'
              icon='mail'
              onChange={this.updateForm}
            />
            <Form.Input
              placeholder='Password'
              name='password'
              type='password'
              icon='lock'
              onChange={this.updateForm}
            />
            <Form.Button color='teal' content='Login' fluid />
            <a href='forgot'>Forgot password?</a>
          </Form>
          <Divider horizontal>Or</Divider>
          <Button
            onClick={() => this.context.router.history.push('signup')}
            color='blue'
            content='Sign Up'
            fluid
          />
        </div>
      </div>
    )
  }
}

LoginView.propTypes = {
  doLogin: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

export default LoginView
