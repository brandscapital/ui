import React, { Component } from 'react'
import propTypes from 'prop-types'
import {
  Header,
  Menu,
  Segment,
  List,
  Modal,
  Icon,
  Feed
} from 'semantic-ui-react'
import Comments from 'components/Comments'
import Snooze from 'components/Snooze'
import { shouldRender } from 'utils/render'

export default class CollectionSidebar extends Component {
  static propTypes = {
    collectionId: propTypes.number.isRequired,
    comments: propTypes.object.isRequired,
    activityTree: propTypes.object.isRequired,
    taskTypes: propTypes.object.isRequired,
    doCreateComment: propTypes.func.isRequired,
    doCreateTask: propTypes.func.isRequired,
    doCreateSnooze: propTypes.func.isRequired
  }

  state = {
    activeItem: 'feed',
    task: null,
    taskMode: false,
    formData: {}
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  setTaskMode = task => {
    this.setState({
      task,
      taskMode: true
    })
  }

  clearTask = () => {
    this.setState({
      task: null,
      taskMode: false
    })
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      formData: {
        ...this.state.formData,
        ...data
      }
    })
  }

  submitTaskForm = () => {
    const { task, formData } = this.state
    const { doCreateTask, collectionId } = this.props
    const newTask = {
      data: JSON.stringify(formData),
      task_type_id: task.get('id'),
      collection_id: collectionId
    }
    doCreateTask(newTask)
  }

  static propTypes = {
    taskTypes: propTypes.object
  }

  renderWorkflow = () => {
    return <Header size='small'>This is the workflow panel.</Header>
  }

  // renderTaskModal = () => {
  //   const { task, formData } = this.state
  //   const viewerProps = {
  //     formData,
  //     onChange: this.updateFormData,
  //     schema: task.getIn(['config', 'schema']).toJS(),
  //     onSubmit: () => this.submitTaskForm()
  //   }
  //
  //   return (
  //     <Modal size='mini' open closeIcon onClose={() => this.clearTask()}>
  //       <Modal.Header>
  //         New Task - {task.get('name')}
  //       </Modal.Header>
  //       <Modal.Content>
  //         <JSONForm {...viewerProps} />
  //       </Modal.Content>
  //     </Modal>
  //   )
  // }

  renderTasks = () => {
    const { taskTypes, doCreateSnooze, collectionId } = this.props
    const { taskMode } = this.state
    const typeIcons = {
      data_entry: 'keyboard',
      notification: 'chat'
    }
    const actionable = taskTypes.map(t => {
      return (
        <List.Item key={t.get('id')}>
          <List.Content floated='right'>
            <Snooze
              collectionId={collectionId}
              taskTypeId={t.get('id')}
              onSubmit={doCreateSnooze}
            />
          </List.Content>
          <List.Content
            verticalAlign='middle'
            as='a'
            onClick={() => this.setTaskMode(t)}
          >
            <Icon name={typeIcons[t.getIn(['config', 'action'])]} />
            {t.get('name')}
          </List.Content>
        </List.Item>
      )
    })
    return (
      <div>
        {taskMode ? this.renderTaskModal() : null}
        <Segment>
          <List>{actionable}</List>
        </Segment>
      </div>
    )
  }

  renderFeed = () => {
    const { activityTree } = this.props
    const taskMeta = {
      userTask: { icon: 'user', name: 'User Task' },
      serviceTask: { icon: 'cog', name: 'Service Task' },
      manualTask: { icon: 'hand paper outline', name: 'Manual Task' }
    }
    const children = activityTree.childActivityInstances.map((v, k) => (
      <Feed.Event key={k}>
        <Feed.Label>
          <Icon name={taskMeta[v.activityType].icon} />
        </Feed.Label>
        <Feed.Content>
          <Feed.Date>{taskMeta[v.activityType].name}</Feed.Date>
          <Feed.Summary>{v.name}</Feed.Summary>
        </Feed.Content>
      </Feed.Event>
    ))
    return (
      <Segment>
        <Feed />
      </Segment>
    )
  }

  renderComments = () => {
    const { comments, doCreateComment, collectionId } = this.props
    return (
      <Comments
        comments={comments}
        createComment={doCreateComment}
        collectionId={collectionId}
      />
    )
  }

  render () {
    const { activeItem } = this.state

    const loadModule = {
      feed: this.renderFeed,
      comments: this.renderComments,
      workflow: this.renderWorkflow,
      tasks: this.renderTasks
    }

    return (
      <div>
        <Menu pointing secondary>
          <Menu.Item
            name='feed'
            content='Activity Feed'
            active={activeItem === 'feed'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='tasks'
            active={activeItem === 'tasks'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='workflow'
            content='Workflow'
            active={activeItem === 'workflow'}
            onClick={this.handleItemClick}
          />
        </Menu>
        <div>{loadModule[activeItem]()}</div>
      </div>
    )
  }
}
