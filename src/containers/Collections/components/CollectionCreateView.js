import React from 'react'
import PropTypes from 'prop-types'
import { Page, Layout, TextStyle, PageActions } from '@shopify/polaris'
import { Form, Divider, Segment, Grid, Label, Menu } from 'semantic-ui-react'

import { ResourceCreateView } from 'components/Views'
import { PageSkeleton } from 'components/Skeletons'
import StartForm from './StartForm'

class CollectionCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    form: {
      workflowId: '',
      files: []
    },
    start: {}
  }

  componentDidMount () {
    const { navigationViewController } = this.props
    navigationViewController.setView('product/documents')
  }

  componentWillUpdate (nextProps, nextState) {
    const { workflowId } = nextState.form
    if (workflowId !== this.state.form.workflowId) {
      const workflow = this.props.dependencies.workflows
        .get(`${workflowId}`)
        .toJS()
      this.props.fetchVars(workflow.bpmnId)
    }
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateStartForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      start: {
        ...this.state.start,
        ...data
      }
    })
  }

  handleSave = () => {
    const { saveAction, resourceName, dependencies } = this.props
    const { form, start } = this.state
    const blobFile = new Blob([JSON.stringify(start, null, 2)], {
      type: 'application/json'
    })
    const newCollection = {
      ...form,
      files: blobFile,
      start,
      workflow: dependencies.workflows.getIn([`${form.workflowId}`, 'bpmnId'])
    }

    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderStartForm = () => {
    const { form: { workflowId, files }, start } = this.state
    const { startVars, workflows, documentTypes } = this.props.dependencies
    const workflow = workflows.get(`${workflowId}`)
    const startForm = startVars.get(`${workflow.bpmnId}`)
    const docType = documentTypes.get(`${workflow.documentTypeId}`)
    return (
      <Layout.Section>
        <StartForm
          workflow={workflow}
          documentType={docType}
          startState={start}
          startForm={startForm}
          onChange={this.updateStartForm}
          files={files}
          onFileChange={this.updateForm}
        />
      </Layout.Section>
    )
  }

  renderContent = () => {
    const { form: { workflowId }, start } = this.state
    const {
      resourceName,
      resourceForm,
      saving,
      dependencies,
      fetching
    } = this.props
    const { workflows, startVars, documentTypes } = dependencies
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    let startValid = false
    for (let v in start) {
      startValid = start[v]
    }
    const isValid = workflowId !== '' && startValid
    const workflowOps = workflows
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    return (
      <Page
        title={`Create ${resourceName.singular}`}
        fullWidth
        breadcrumbs={breadcrumbs}
      >
        <Layout>
          <Layout.Section>
            <Segment loading={saving || fetching}>
              <Form size='huge' onSubmit={this.handleSave}>
                <Form.Group widths='equal'>
                  <Form.Dropdown
                    label='Workflow'
                    name='workflowId'
                    placeholder='Choose a workflow'
                    search
                    selection
                    options={workflowOps}
                    value={workflowId}
                    required={true}
                    onChange={this.updateForm}
                  />
                </Form.Group>
              </Form>
            </Segment>
          </Layout.Section>
          {!fetching && workflowId && startVars.count()
            ? this.renderStartForm()
            : null}
          <Layout.Section>
            <PageActions
              primaryAction={{
                content: `Save ${resourceName.singular}`,
                onClick: () => this.handleSave(),
                disabled: !isValid
              }}
            />
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}

export default CollectionCreateView
