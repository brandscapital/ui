import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/collections'
import CollectionListItem from './components/CollectionListItem'
import EditContainer from './Edit'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.collections.get('list'),
    resourceName,
    navView: 'product/documents',
    itemRenderer: CollectionListItem,
    detailView: EditContainer,
    loading:
      state.loading.effects.collections.fetchAll ||
      state.loading.effects.workflows.fetchAll ||
      false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    primaryAction: [
      { content: 'Create Collection', onClick: () => dispatch.collections.create() }
    ],
    prefetch: [
      () => dispatch.collections.fetchAll(),
      () => dispatch.workflows.fetchAll()
    ],
    refetch: () => dispatch.collections.fetchAll(),
    removeAction: id => dispatch.collections.remove(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
