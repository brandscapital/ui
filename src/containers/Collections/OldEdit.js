import { connect } from 'react-redux'
import EditCollectionView from './components/EditCollectionView'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

const mapStateToProps = (state, { match }) => {
  const collection = state.collections.getIn(['list', `${match.params.id}`])
  const process = collection
    ? state.processes.getIn(['list', `${collection.processInstanceId}`])
    : null
  const workflow = collection
    ? state.workflows.getIn(['list', `${collection.workflowId}`])
    : null
  const loadT = state.loading.effects.collections
  const loadP = state.loading.effects.processes
  const loadW = state.loading.effects.workflows
  const isLoadingProcess =
    loadP.fetch || loadP.fetchVariables || loadP.fetchActivityTree
  return {
    collection,
    process,
    workflow,
    loading: isLoadingProcess || loadT.fetch || loadW.fetch
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    doUpdateCollection: collection => dispatch.collections.update(collection),
    doCreateComment: (collection, comment) =>
      dispatch.comments.create(collection, comment),
    doCreateTask: data => dispatch.tasks.create(data),
    doCreateSnooze: data => dispatch.tasks.create(data),
    fetchData: async () => {
      let collection = await dispatch.collections.fetch({ id: match.params.id })
      let processId = await collection.processInstanceId
      let workflowId = await collection.workflowId
      let proc = await dispatch.processes.fetch(processId)
      dispatch.processes.fetchVariables(proc.id)
      dispatch.processes.fetchActivityTree(proc.id)
      dispatch.workflows.fetch({ id: workflowId })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(EditCollectionView)
)
