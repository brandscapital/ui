import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Header,
  Segment,
  Grid,
  Label,
  Menu,
  Message,
  List,
  Icon,
  Button
} from 'semantic-ui-react'

import { ResourceEditView } from 'components/Views'
import { ContainerLoader } from 'components/Loader'
import ResourceNotFound from 'components/Views/ResourceNotFound'

class CompleteTaskView extends ResourceEditView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      form: {
        formVariables: props.item && props.item.formVariables
          ? props.item.formVariables.toJS()
          : null
      }
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      const { formVariables } = nextProps.item
      this.setState({
        form: {
          formVariables: formVariables ? formVariables : formVariables.toJS()
        }
      })
    }
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }


  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newCollection = {
      ...form
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const { form } = this.state
    const {
      saving,
      dependencies,
      item
    } = this.props
    const isValid = true

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Task Form - {item.name}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <pre>{JSON.stringify(dependencies.taskVariables, null, 2)}</pre>
            </Segment>
            <Button floated='right' disabled={!isValid} positive content='Save' onClick={() => this.handleSave()} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  render () {
    const { loading, saving, item, dependencies: { taskVariables} } = this.props
    if (loading && !saving) {
      return <ContainerLoader />
    } else {
      return item && taskVariables ? this.renderContent() : <ResourceNotFound />
    }
  }
}

export default CompleteTaskView
