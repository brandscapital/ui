import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/tasks'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/TasksListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.tasks.get('list'),
    dependencies: {
      users: state.users.get('list')
    },
    loading: (
      state.loading.effects.tasks.fetchAll ||
      state.loading.effects.users.fetchAll || false
    ),
    resourceName,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    hideIndex: true,
    navView: 'product/tasks'
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
      () => dispatch.users.fetchAll(),
      () => dispatch.tasks.fetchAll()
    ],
    shortcutActions: item => [
      {
        text: 'Claim',
        icon: 'hand paper outline',
        onClick: () => {
          console.log(item.id)
          dispatch.tasks.claimTask(item.id)
        }
      }
    ],
    refetch: ops => dispatch.tasks.fetchAll()
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
