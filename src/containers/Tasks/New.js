import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/tasks'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      users: state.users
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'product/tasks',
    loading: (
      state.loading.effects.users.fetchAll || false
    ),
    saving: state.loading.effects.tasks.create || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: task => dispatch.tasks.update(task),
    prefetch: [
      () => dispatch.tasks.fetch({ id }),
      () => dispatch.users.fetchAll()
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceCreateView)
)
