import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/tasks'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.tasks.getIn(['list', `${match.params.id}`]),
    dependencies: {
      users: state.users
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'product/tasks',
    loading: (
      state.loading.effects.tasks.fetch
    ),
    saving: state.loading.effects.tasks.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: task => dispatch.tasks.update(task),
    prefetch: [
      () => dispatch.tasks.fetch({ id })
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceEditView)
)
