import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import CompleteTaskView from './components/CompleteTaskView'
import { modelConfig } from 'models/tasks'

const mapStateToProps = (state, { match }) => {
  const { resourceName } = modelConfig
  return {
    item: state.tasks.getIn(['list', `${match.params.id}`]),
    dependencies: {
      taskVariables: state.taskVariables.getIn(['list', `${match.params.id}`])
    },
    loading: (
      state.loading.effects.tasks.fetch ||
      state.loading.effects.taskVariables.fetch
    ) || false,
    resourceName: {
      singular: resourceName.singular,
      plural: `Task ${resourceName.plural}`,
      route: resourceName.route
    },
    navView: 'product/home'
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    prefetch: [
      () => dispatch.tasks.fetch({ id }),
      () => dispatch.taskVariables.fetch(id)
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(CompleteTaskView)
)
