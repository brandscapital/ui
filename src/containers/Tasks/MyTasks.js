import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/tasks'
import store from 'store'
import EditContainer from './Complete'
import itemRenderer from './components/MyTaskListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  const authUser = state.auth.get('user')
  return {
    items: state.tasks.get('list').filter(t => t.assignee === `${authUser.id}`),
    dependencies: {
      authUser
    },
    itemRenderer,
    detailView: EditContainer,
    loading: state.loading.models.tasks || false,
    resourceName: {
      singular: resourceName.singular,
      plural: `My ${resourceName.plural}`,
      route: `/my-tasks`
    },
    navView: 'product/home'
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  const state = store.getState()
  const params = { assignee: `${state.auth.user.id}` }
  return {
    removeAction: id => dispatch.tasks.remove(id),
    prefetch: [
      () => dispatch.users.fetchAll(),
      () => dispatch.tasks.fetchAll(params)
    ],
    refetch: ops => dispatch.tasks.fetchAll(),
    claimTask: id => dispatch.tasks.claimTask(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
