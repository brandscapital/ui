import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/roles'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RolesListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.roles.get('list'),
    dependencies: {
      permissions: state.permissions.get('list')
    },
    resourceName,
    navView: 'product/roles',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: (
      state.loading.effects.roles.fetchAll
    )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
      () => dispatch.roles.fetchAll()
    ],
    refetch: ops => dispatch.workflows.fetchAll(ops),
    removeAction: id => dispatch.roles.remove(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
