import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/roles'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      permissions: state.permissions
        .get('list')
    },
    resourceName,
    resourceForm,
    navView: 'product/roles',
    loading: state.loading.effects.permissions.fetchAll,
    saving: state.loading.effects.roles.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: role => dispatch.roles.create(role),
    prefetch: [() => dispatch.permissions.fetchAll()]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceCreateView)
)
