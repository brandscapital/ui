import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/roles'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.roles.getIn(['list', `${match.params.id}`]),
    dependencies: {
      permissions: state.permissions
        .get('list').toList()
    },
    resourceName,
    resourceForm,
    navView: 'product/roles',
    loading: (
      state.loading.effects.permissions.fetchAll ||
      state.loading.effects.roles.fetch
    ),
    saving: state.loading.effects.roles.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = { with: 'permissions' }
  return {
    saveAction: role => dispatch.roles.update(role),
    prefetch: [
      () => dispatch.roles.fetch({ id, params }),
      () => dispatch.permissions.fetchAll()
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceEditView)
)
