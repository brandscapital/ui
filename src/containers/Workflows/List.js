import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/workflows'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/WorkflowListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: state.workflows.get('list'),
    resourceName,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'product/workflows',
    loading: state.loading.models.workflows || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.workflows.fetchAll()],
    refetch: ops => dispatch.workflows.fetchAll(ops),
    removeAction: id => dispatch.workflows.remove(id),
    shortcutActions: item => [
      {
        text: 'Clone',
        onClick: () => dispatch.workflows.duplicate(item.id),
        icon: 'clone outline'
      }
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
