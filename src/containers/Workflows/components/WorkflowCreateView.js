import React from 'react'
import PropTypes from 'prop-types'
import { Form, Divider, Segment, Grid, Label, Message } from 'semantic-ui-react'
import Drawer from '@atlaskit/drawer'

import { ResourceCreateView } from 'components/Views'
import WorkflowEditor from 'components/WorkflowEditor'

class WorkflowCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  state = {
    form: {
      documentTypeId: '',
      options: {
        startInstanceOnUpload: false
      }
    }
  }

  static contextTypes = {
    router: PropTypes.object
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateOptions = (e, { name }) => {
    const { options } = this.state.form
    options[name] = !options[name]
    this.setState({
      form: {
        ...this.state.form,
        ...{ options }
      }
    })
  }

  onSave = ({ xml, defs }) => {
    const { name, id } = defs.rootElements[0]
    const { form } = this.state
    const { saveAction, resourceName } = this.props
    const newWorkflow = {
      ...form,
      bpmnXml: xml,
      bpmnId: id,
      name: name || id
    }
    saveAction(newWorkflow).then(data => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  openDrawer = () =>
    this.setState({
      isDrawerOpen: true
    })

  closeDrawer = () =>
    this.setState({
      isDrawerOpen: false
    })

  renderContent = () => {
    const { isDrawerOpen, form } = this.state
    const { resourceName, resourceForm, saving, dependencies } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const documentTypeOps = dependencies.documentTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()
    const isValid = form.documentTypeId

    return (
      <div className='workflow-container'>
        <Drawer onClose={this.closeDrawer} isOpen={isDrawerOpen}>
          <div className='drawer-content'>
            <Form size='small'>
              <Form.Dropdown
                label='Start Document Type'
                name='documentTypeId'
                placeholder='Choose a document type'
                options={documentTypeOps}
                value={form.documentTypeId}
                required
                search
                selection
                onChange={this.updateForm}
              />
              <Form.Checkbox
                toggle
                name='startInstanceOnUpload'
                label='Start new instance on creation'
                checked={form.options.startInstanceOnUpload}
                onChange={this.updateOptions}
              />
            </Form>
          </div>
        </Drawer>
        <WorkflowEditor
          onSave={isValid ? this.onSave : null}
          openDrawer={this.openDrawer}
        />
      </div>
    )
  }
}

export default WorkflowCreateView
