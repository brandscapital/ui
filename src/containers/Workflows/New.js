import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'
import _ from 'lodash'

import WorkflowCreateView from './components/WorkflowCreateView'
import { modelConfig } from 'models/workflows'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      documentTypes: state.documentTypes.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'product/workflows',
    loading:
      state.loading.models.workflows ||
      state.loading.models.documentTypes ||
      false,
    saving:
      state.loading.effects.workflows.update ||
      state.loading.effects.workflows.unpublish ||
      state.loading.effects.workflows.publish ||
      state.loading.effects.workflows.duplicate ||
      false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    prefetch: [() => dispatch.documentTypes.fetchAll()],
    saveAction: workflow => dispatch.workflows.create(workflow)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(WorkflowCreateView)
)
