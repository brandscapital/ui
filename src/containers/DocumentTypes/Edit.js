import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'
import * as fileTypes from 'utils/fileTypes'

import DocumentTypeEditView from './components/DocumentTypeEditView'
import { modelConfig } from 'models/documentTypes'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.documentTypes.getIn(['list', `${match.params.id}`]),
    dependencies: {
      fileTypes
    },
    resourceName,
    resourceForm,
    navView: 'product/documents',
    saving: state.loading.effects.documentTypes.update || false,
    loading: state.loading.effects.documentTypes.fetch || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: type => dispatch.documentTypes.update(type),
    prefetch: [() => dispatch.documentTypes.fetch({ id })]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(DocumentTypeEditView)
)
