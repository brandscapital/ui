import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documentTypes'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/DocumentTypesListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig
  return {
    items: state.documentTypes.get('list'),
    resourceName,
    resourceList: resourceList(state),
    navView: 'product/documents',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: state.loading.effects.documentTypes.fetchAll || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.documentTypes.fetchAll()],
    refetch: ops => dispatch.documentTypes.fetchAll(ops),
    removeAction: id => dispatch.documentTypes.remove(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
