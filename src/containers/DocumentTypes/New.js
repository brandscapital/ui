import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'
import * as fileTypes from 'utils/fileTypes'

import DocumentTypeCreateView from './components/DocumentTypeCreateView'
import { modelConfig } from 'models/documentTypes'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      fileTypes
    },
    resourceName,
    resourceForm,
    navView: 'product/documents',
    saving: state.loading.effects.documentTypes.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [],
    saveAction: type => dispatch.documentTypes.create(type)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(DocumentTypeCreateView)
)
