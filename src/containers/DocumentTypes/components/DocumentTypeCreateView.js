import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Header,
  Segment,
  Grid,
  Button,
  Menu,
  Message,
  Icon,
  List,
  Label
} from 'semantic-ui-react'

import { ResourceCreateView } from 'components/Views'
import EmptyFileTypes from './EmptyFileTypes'

class DocumentTypeCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    form: {
      name: '',
      description: '',
      properties: {
        fileTypes: []
      }
    },
    activeMenuItem: 'image'
  }

  componentDidMount () {
    const { navigationViewController } = this.props
    navigationViewController.setView('product/documents')
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  addFileType = (e, { name }) => {
    const { form } = this.state
    form.properties.fileTypes.push(name)
    this.setState({
      form: {
        ...this.state.form,
        ...form
      }
    })
  }

  removeFileType = id => {
    const { form } = this.state
    form.properties.fileTypes.splice(id, 1)
    this.setState({
      form: {
        ...this.state.form,
        ...form
      }
    })
  }

  menuClick = (e, { name }) => this.setState({ activeMenuItem: name })

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newCollection = {
      ...form
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderContent = () => {
    const {
      activeMenuItem,
      form: { name, description, properties }
    } = this.state
    const { saving, dependencies } = this.props
    const isValid =
      name !== '' && description !== '' && properties.fileTypes.length > 0

    const listItems = dependencies.fileTypes[activeMenuItem]
      .filter(t => !properties.fileTypes.includes(t.id))
      .map((r, k) => (
        <List.Item
          icon='file'
          name={r.id}
          header={r.name}
          description={r.filenames.join(', ')}
          key={r.id}
        />
      ))

    const fileTypeLabels = properties.fileTypes.map((v, k) => (
      <Label key={k} as='a' onClick={() => this.removeFileType(k)}>
        {dependencies.fileTypes.all.find(f => f.id === v)['name']}
        <Icon name='close' />
      </Label>
    ))

    const menuItems = [
      {
        key: 'image',
        active: activeMenuItem === 'image',
        name: 'image',
        onClick: this.menuClick
      },
      {
        key: 'video',
        active: activeMenuItem === 'video',
        name: 'video',
        onClick: this.menuClick
      },
      {
        key: 'audio',
        active: activeMenuItem === 'audio',
        name: 'audio',
        onClick: this.menuClick
      },
      {
        key: 'text',
        active: activeMenuItem === 'text',
        name: 'text',
        onClick: this.menuClick
      },
      {
        key: 'application',
        active: activeMenuItem === 'application',
        name: 'application',
        onClick: this.menuClick
      }
    ]

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Create Document Type</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <Form size='small'>
                <Form.Group widths='equal'>
                  <Form.Input
                    label='Name'
                    name='name'
                    placeholder='Name of document type'
                    required
                    value={name}
                    onChange={this.updateForm}
                  />
                  <Form.Input
                    label='Description'
                    name='description'
                    placeholder='Description of document type'
                    required
                    value={description}
                    onChange={this.updateForm}
                  />
                </Form.Group>
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Message
              size='small'
              content='Choose file types from the lists below that this document type should allow'
              info
              icon='info circle'
              attached='top'
            />
            <Segment loading={saving} attached='bottom'>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column width={7}>
                    <Header size='small'>Selected File Types</Header>
                    {fileTypeLabels.length ? (
                      <Label.Group size='small'>{fileTypeLabels}</Label.Group>
                    ) : (
                      <EmptyFileTypes />
                    )}
                  </Grid.Column>
                  <Grid.Column width={9}>
                    <Header size='small'>Available File Types</Header>
                    <Menu size='mini' attached='top' items={menuItems} />
                    <Segment
                      attached='bottom'
                      style={{ height: 220, overflowY: 'auto' }}
                    >
                      <List
                        items={listItems}
                        onItemClick={this.addFileType}
                        selection
                        animated
                        verticalAlign='middle'
                        size='tiny'
                      />
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
            <Button floated='right' disabled={!isValid} positive content='Save' onClick={() => this.handleSave()} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default DocumentTypeCreateView
