import React from 'react'
import { Form, Segment, Header, Icon } from 'semantic-ui-react'

const EmptyFileTypes = () => (
  <Segment placeholder>
    <Header size='small' icon>
      <Icon color='blue' name='info circle' />
      No file types selected. Try adding from the right menu.
    </Header>
  </Segment>
)

export default EmptyFileTypes
