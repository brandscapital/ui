import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceCreateView } from 'components/Views'
import { modelConfig } from 'models/users'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      roles: state.roles
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'product/users',
    loading: state.loading.effects.roles.fetchAll || false,
    saving: state.loading.effects.users.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: user => dispatch.users.create(user),
    prefetch: [() => dispatch.roles.fetchAll()]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceCreateView)
)
