import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/users'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.users.getIn(['list', `${match.params.id}`]),
    dependencies: {
      roles: state.roles
        .get('list')
        .toList()
        .toJS()
    },
    resourceName,
    resourceForm,
    navView: 'product/users',
    loading: (
      state.loading.effects.roles.fetchAll ||
      state.loading.effects.users.fetch
    ),
    saving: state.loading.effects.users.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = { with: 'roles' }
  return {
    saveAction: user => dispatch.users.update(user),
    prefetch: [
      () => dispatch.users.fetch({ id, params }),
      () => dispatch.roles.fetchAll()
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceEditView)
)
