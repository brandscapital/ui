import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/users'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/UsersListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.users.get('list'),
    resourceName,
    navView: 'product/users',
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    loading: state.loading.effects.users.fetchAll || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    removeAction: id => dispatch.users.remove(id),
    refetch: ops => dispatch.users.fetchAll(ops),
    prefetch: [() => dispatch.users.fetchAll()]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
