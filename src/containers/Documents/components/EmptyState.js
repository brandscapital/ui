import React from 'react'
import { Button, Segment, Header, Icon } from 'semantic-ui-react'

const EmptyFileTypes = ({ message, add, action }) => (
  <Segment placeholder>
    <Header icon>
      <Icon color='blue' name='info circle' />
      {message}
    </Header>
    {action && add ? (
      <Button
        primary
        onClick={() => add()}
        content={action}
      />
    ) : null}
  </Segment>
)

export default EmptyFileTypes
