import React from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Header,
  Segment,
  Grid,
  Button,
  Menu,
  Message,
  Icon
} from 'semantic-ui-react'
import Uppy from '@uppy/core'
import { Dashboard } from '@uppy/react'
import S3Uploader from '@uppy/aws-s3'

import api from 'middleware/api'
import { ResourceCreateView } from 'components/Views'

class DocumentCreateView extends ResourceCreateView {
  constructor (props) {
    super(props)
    this.uppy = new Uppy({
      debug: true
    })
      .use(S3Uploader, {
        getUploadParameters (file) {
          // Send a request to our PHP signing endpoint.
          return api
            .post('/s3-sign', {
              filename: encodeURIComponent(file.name),
              contentType: file.type
            })
            .then(({ data }) => {
              // Return an object in the correct shape.
              return {
                method: 'PUT',
                url: data.url,
                headers: {
                  'content-type': file.type,
                  'content-disposition': `inline`
                },
                fields: []
              }
            })
        }
      })
      .on('file-added', file => {
        file.name = file.name.replace(/[\s\uFEFF\xA0]/g, '_')
        file.meta.name = file.name
        this.addFile(file)
      })
      .on('file-removed', file => {
        this.removeFile(file)
      })
      .on('upload-success', (file, data) => {
        const path = data.location.split('.com')
        this.updateFile(null, { name: 'path', value: path[1], id: file.id })
      })
  }

  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    files: []
  }

  componentWillUnmount () {
    this.uppy.close()
  }

  updateFile = (e, { name, value, id }) => {
    let data = this.state.files
    const idx = data.findIndex(f => f.id === id)
    let file = data[idx]
    file[name] = value
    data[idx] = file
    this.setState({
      files: data
    })
  }

  addFile = info => {
    let data = this.state.files
    data.push({ path: '', info, documentTypeId: '', id: info.id })
    this.setState({
      files: data
    })
  }

  removeFile = file => {
    let data = this.state.files
    const idx = data.findIndex(f => f.id === file.id)
    data.splice(idx, 1)
    this.setState({
      files: data
    })
  }

  handleSave = () => {
    const { saveAction, resourceName } = this.props
    const { files } = this.state
    const newCollection = {
      files
    }
    saveAction(newCollection).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderTypes = () => {
    const { files } = this.state
    const { dependencies } = this.props

    return (
      <Form size='small'>
        {files.map((v, k) => {
          let ops = dependencies.documentTypes
            .filter(f =>
              f.getIn(['properties', 'fileTypes']).contains(v.info.type)
            )
            .map(r => ({
              key: r.id,
              text: r.name,
              value: r.id
            }))
            .toList()
            .toJS()
          return (
            <Form.Dropdown
              label={v.info.name}
              name='documentTypeId'
              placeholder={
                ops.length
                  ? 'Choose document type'
                  : 'No document types for this file type'
              }
              selection
              search
              options={ops}
              id={v.id}
              key={k}
              disabled={ops.length === 0}
              value={v.documentTypeId}
              onChange={this.updateFile}
            />
          )
        })}
      </Form>
    )
  }

  renderContent = () => {
    const { files } = this.state
    const { resourceName, resourceForm, saving, dependencies } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = files.length > 0
    const uppy = this.uppy
    const dashProps = {
      uppy,
      height: 400,
      thumbnailWidth: 180,
      showProgressDetails: true,
      browserBackButtonClose: false,
      proudlyDisplayPoweredByUppy: false
    }
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Create Document(s)</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Message
              size='small'
              info
              icon='info circle'
              list={[
                'Document types that support each file type can be set after adding files',
                'Files uploaded without a document type can be modified later on "Pending Files" page'
              ]}
              attached='top'
            />
            <Segment padded attached='bottom' loading={saving}>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column textAlign='center' width={10}>
                    <Dashboard {...dashProps} />
                  </Grid.Column>
                  <Grid.Column width={6}>
                    {files.length > 0 ? this.renderTypes() : null}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
            <Button floated='right' disabled={!isValid} positive content='Save' onClick={() => this.handleSave()} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default DocumentCreateView
