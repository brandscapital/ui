import React, { Component } from 'react'
import { List, Tab, Menu, Header, Grid, Step } from 'semantic-ui-react'

import EmptyState from './EmptyState'

const activityTypeMap = {
  userTask: { name: 'User Task', icon: 'user' }
}

class ProcessTab extends Component {
  state = {
    activeItem: 0
  }

  handleItemClick = (id) => this.setState({ activeItem: id })

  componentDidMount () {
    const { processes, services } = this.props
    if (processes && processes.count()) {
      const id = processes.getIn([0, 'processInstanceId'])
      services.fetch(id)
      services.fetchActivityTree(id)
      services.fetchVariables(id)
    }
  }

  renderList = () => {
    const { processes, workflows } = this.props
    const { activeItem } = this.state
    const items = processes.map((v, k) => (
      <List.Item
        key={k}
        active={activeItem === k}
        onClick={() => this.handleItemClick(k)}
        content={workflows.getIn([`${v.workflowId}`, 'name'])}
      />
    )).toJS()

    const activityTree = processes.getIn([activeItem, 'activity'])
    const activityList = []
    if (activityTree) {
      activityList.push({
        key: 'processDefintion',
        completed: true,
        title: activityTree.get('activityName'),
        description: 'Process Start',
        icon: 'play'
      })
      activityTree.get('childActivityInstances').forEach((c, k) => {
        activityList.push({
          key: c.get('activityId'),
          title: c.get('activityName'),
          description: activityTypeMap[c.get('activityType')].name,
          icon: activityTypeMap[c.get('activityType')].icon
        })
      })
      activityList[(activityList.length - 1)].active = true
    }
    return (
      <Grid columns={2}>
        <Grid.Column width={5}>
          <List verticalAlign='middle' size='tiny' selection items={items} />
        </Grid.Column>
        <Grid.Column width={11}>
          <Header>Progress</Header>
          <Step.Group size='mini' items={activityList} />
        </Grid.Column>
      </Grid>
    )
  }

  render () {
    const { processes, loading } = this.props
    return (
      <Tab.Pane loading={loading} attached='bottom' padded>
        {processes.count() ? this.renderList() : <EmptyState message='No processes linked to this document.' /> }
      </Tab.Pane>
    )
  }
}

export default ProcessTab
