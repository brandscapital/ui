import React from 'react'
import PropTypes from 'prop-types'
import {
  Header,
  Segment,
  Grid,
  Menu,
  Label,
  Tab
} from 'semantic-ui-react'
import { withNavigationViewController } from '@atlaskit/navigation-next'

import { BaseView } from 'components/Views'
import ProcessTab from './ProcessTab'

class DocumentView extends BaseView {
  static propTypes = {
    item: PropTypes.object,
    navView: PropTypes.string,
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.item !== nextProps.item) {
      this.setState({
        form: {
          id: nextProps.item.id,
          documentTypeId: nextProps.item.documentTypeId,
          filename: nextProps.item.filename
        }
      })
    }
  }

  renderContent = () => {
    const { resourceName, procLoading, dependencies, item, processServices } = this.props
    const secAction = {
      content: 'View File',
      icon: 'external',
      onClick: () => window.open(item.url)
    }

    const docType = dependencies.documentTypes.getIn([
      `${item.documentTypeId}`,
      'name'
    ])

    const procs = dependencies.processes.filter(p => item.processes.includes(p.processInstanceId)).toList()
    const workflows = dependencies.workflows
    const panes = [
      {
        menuItem: () => (
          <Menu.Item key='processes'>
            Procesess <Label color='blue'>{procs.count()}</Label>
          </Menu.Item>
        ),
        render: () => (
          <ProcessTab
            processes={procs}
            services={processServices}
            loading={procLoading}
            workflows={workflows}
          />
        )
      }
    ]

    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Document - {`${item.id}`}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Menu size='small' borderless attached='top'>
              <Menu.Item>
                <Header
                  color='blue'
                  icon='list alternate outline'
                  content='Details'
                />
              </Menu.Item>
              <Menu.Item {...secAction} />
            </Menu>
            <Segment attached='bottom' size='small' padded>
              <Grid columns={3}>
                <Grid.Column>
                  <Header textAlign='left' sub content='File Name' />
                  <span>{`${item.filename}.${item.extension}`}</span>
                </Grid.Column>
                <Grid.Column>
                  <Header textAlign='left' sub content='MIME Type' />
                  <span>{item.mimeType}</span>
                </Grid.Column>
                <Grid.Column>
                  <Header textAlign='left' sub content='Document Type' />
                  <span>{docType}</span>
                </Grid.Column>
              </Grid>
            </Segment>
            <Tab menu={{ attached: 'top', size: 'small' }} panes={panes} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default DocumentView
