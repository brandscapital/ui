import { connect } from 'react-redux'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documents'
import DocumentListItem from './components/DocumentListItem'
import EditContainer from './Edit'
import CreateContainer from './New'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.documents.get('list').filter(d => d.documentTypeId !== null),
    dependencies: {
      documentTypes: state.documentTypes.get('list')
    },
    resourceName,
    navView: 'product/documents',
    itemRenderer: DocumentListItem,
    detailView: EditContainer,
    createView: CreateContainer,
    loading:
      state.loading.effects.documents.fetchAll ||
      false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  return {
    prefetch: [() => dispatch.documents.fetchAll()],
    removeAction: id => dispatch.documents.remove(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MasterListView)
