import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/documents'
import PendingListItem from './components/PendingListItem'
import EditContainer from './PendingEdit'
import CreateContainer from './New'

const mapStateToProps = (state, ownProps) => {
  const { resourceName } = modelConfig
  return {
    items: state.documents.get('list').filter(d => d.documentTypeId === null),
    resourceName: {
      ...resourceName,
      ...{ plural: 'Pending Files', route: '/pending-files' }
    },
    itemRenderer: PendingListItem,
    detailView: EditContainer,
    createView: CreateContainer,
    navView: 'product/documents',
    loading: state.loading.effects.documents.fetchPending || false,
    disableAction: true
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [() => dispatch.documents.fetchPending()],
    removeAction: id => dispatch.documents.removePending(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
