import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import DocumentView from './components/DocumentView'
import { modelConfig } from 'models/documents'

const mapStateToProps = (state, { match }) => {
  const { resourceName } = modelConfig
  return {
    item: state.documents.getIn(['list', `${match.params.id}`]),
    dependencies: {
      documentTypes: state.documentTypes.get('list'),
      processes: state.processes.get('list'),
      workflows: state.workflows.get('list')
    },
    resourceName,
    navView: 'product/documents',
    loading: (
      state.loading.effects.documents.fetch ||
      state.loading.effects.workflows.fetchAll
    ),
    procLoading: state.loading.models.processes
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  const params = { with: 'processes;document_type' }
  return {
    prefetch: [
      () => dispatch.documents.fetch({ id, params }),
      () => dispatch.workflows.fetchAll()
    ],
    processServices: {
      fetch: (id) => dispatch.processes.fetch(id),
      fetchActivityTree: (id) => dispatch.processes.fetchActivityTree(id),
      fetchVariables: (id) => dispatch.processes.fetchVariables(id)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(DocumentView)
)
