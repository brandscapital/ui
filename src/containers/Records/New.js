import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import RecordCreateView from './components/RecordCreateView'
import { modelConfig } from 'models/records'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    dependencies: {
      recordTypes: state.recordTypes.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'product/records',
    loading: state.loading.effects.recordTypes.fetchAll || false,
    saving: state.loading.effects.records.create || false
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAction: record => dispatch.records.create(record),
    prefetch: [() => dispatch.recordTypes.fetchAll()]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(RecordCreateView)
)
