import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import RecordEditView from './components/RecordEditView'
import { modelConfig } from 'models/records'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.records.getIn(['list', `${match.params.id}`]),
    dependencies: {
      recordTypes: state.recordTypes.get('list')
    },
    resourceName,
    resourceForm,
    navView: 'product/records',
    loading: (
      state.loading.effects.records.fetch ||
      state.loading.effects.recordTypes.fetchAll || false
    ),
    saving: state.loading.effects.records.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: (record) => dispatch.records.update(record),
    prefetch: [
      () => dispatch.records.fetch({ id }),
      () => dispatch.recordTypes.fetchAll()
    ]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(RecordEditView)
)
