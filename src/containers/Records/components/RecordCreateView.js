import React from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Header, Segment, Grid, Label, Message } from 'semantic-ui-react'

import { ResourceCreateView } from 'components/Views'
import { shouldRender } from 'utils/render'
import JSONForm from 'components/JSONschema/JSONForm'
import ErrorList from 'components/JSONschema/ErrorList'

class RecordCreateView extends ResourceCreateView {
  static propTypes = {
    navView: PropTypes.string,
    resourceName: PropTypes.shape({
      singular: PropTypes.string,
      plural: PropTypes.string
    }),
    resourceForm: PropTypes.object,
    prefetch: PropTypes.array,
    loading: PropTypes.bool,
    saveAction: PropTypes.func,
    saving: PropTypes.bool,
    page: PropTypes.object,
    dependencies: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    form: {
      recordTypeId: ''
    },
    data: ''
  }

  componentDidMount () {
    const { navigationViewController } = this.props
    navigationViewController.setView('product/records')
  }

  shouldComponentUpdate (nextProps, nextState) {
    return shouldRender(this, nextProps, nextState)
  }

  componentWillUpdate (nextProps, nextState) {
    const { recordTypeId } = nextState.form
    if (recordTypeId && recordTypeId !== this.state.form.recordTypeId) {
      const recordProps = this.props.dependencies.recordTypes.getIn([
        `${recordTypeId}`,
        'schema',
        'properties'
      ])

      const data = {}
      recordProps.keySeq().forEach((v, k) => (data[v] = ''))
      this.setState({
        data
      })
    }
  }

  updateForm = (e, { name, value }) => {
    const data = {}
    data[name] = value
    this.setState({
      form: {
        ...this.state.form,
        ...data
      }
    })
  }

  updateFormData = ({ formData }) => {
    let data = formData
    Object.keys(data).forEach(
      key => data[key] === undefined && delete data[key]
    )
    this.setState({
      data
    })
  }

  handleSave = ({ formData }) => {
    const { saveAction, resourceName } = this.props
    const { form } = this.state
    const newRecord = {
      ...form,
      data: formData
    }
    saveAction(newRecord).then(() => {
      this.context.router.history.push(`${resourceName.route}`)
    })
  }

  renderSchemaBuilder = () => {
    const { data, form } = this.state
    const { recordTypes } = this.props.dependencies
    const { saving } = this.props
    const recordType = recordTypes.get(`${form.recordTypeId}`).toJS()
    const viewerProps = {
      formData: data,
      schema: recordType.schema,
      uiSchema: recordType.uiSchema,
      liveValidate: true,
      onSubmit: this.handleSave,
      ErrorList,
      className: 'ui tiny form'
    }

    return (
      <Grid columns={1}>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <Label attached='top left' content='Record Data' />
              <JSONForm {...viewerProps} />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  renderContent = () => {
    const { recordTypeId } = this.state.form
    const { data } = this.state
    const { resourceName, resourceForm, saving, dependencies } = this.props
    const breadcrumbs = [
      { content: `${resourceName.plural}`, url: `${resourceName.route}` }
    ]
    const isValid = recordTypeId !== ''
    const typeOps = dependencies.recordTypes
      .map(r => ({
        key: r.id,
        text: r.name,
        value: r.id
      }))
      .toList()
      .toJS()

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h1'>Create Record</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment loading={saving}>
              <Form size='small' onSubmit={this.handleSave}>
                <Form.Dropdown
                  label='Record Type'
                  type='text'
                  name='recordTypeId'
                  placeholder='Choose a record type'
                  value={recordTypeId}
                  selection
                  required
                  onChange={this.updateForm}
                  options={typeOps}
                />
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {recordTypeId && data ? (
              this.renderSchemaBuilder()
            ) : (
              <Message
                icon='info circle'
                header='No Record Type Selected'
                content='Select a record type to create...'
              />
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default RecordCreateView
