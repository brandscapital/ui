import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/records'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/RecordsListItem'

const mapStateToProps = (state, ownProps) => {
  const { resourceName, resourceList } = modelConfig

  return {
    items: state.records.get('list'),
    dependencies: {
      recordTypes: state.recordTypes.get('list')
    },
    resourceName,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'product/records',
    loading: (
      state.loading.effects.records.fetchAll ||
      state.loading.effects.recordTypes.fetchAll || false
    )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    prefetch: [
      () => dispatch.records.fetchAll(),
      () => dispatch.recordTypes.fetchAll()
    ],
    removeAction: id => dispatch.records.remove(id),
    refetch: ops => dispatch.records.fetchAll(ops)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(MasterListView)
)
