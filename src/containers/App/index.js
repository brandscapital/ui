import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, withRouter } from 'react-router'
import { connect } from 'react-redux'
import { colors } from '@atlaskit/theme'
import {
  LayoutManagerWithViewController,
  modeGenerator,
  withNavigationViewController,
  ThemeProvider
} from '@atlaskit/navigation-next'
import ReduxToastr from 'react-redux-toastr'

import {
  GlobalNav,
  ProductHomeNavView,
  ProductDocumentsNavView,
  ProductUsersNavView,
  ProductWorkflowsNavView,
  ProductRecordsNavView,
  ProductRolesNavView,
  ProductTasksNavView
} from 'components/Navigation'
import { AppLoader } from 'components/Loader'
import HomeContainer from '../Home'
import { ListCollections } from '../Collections'
import { ListDocuments, ListPendingDocuments } from '../Documents'
import { ListDocTypes } from '../DocumentTypes'
import { ListUsers } from '../Users'
import { ListWorkflows } from '../Workflows'
import { ListRecordTypes } from '../RecordTypes'
import { ListRecords } from '../Records'
import { ListRoles } from '../Roles'
import { ListPermissions } from '../Permissions'
import { ListTasks, MyTasks } from '../Tasks'

const globalTheme = modeGenerator({
  product: {
    text: colors.N0,
    background: colors.G300
  },
  global: {
    text: colors.N0,
    background: colors.G400
  }
})

class App extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount () {
    const { navigationViewController, createCollection } = this.props
    navigationViewController.addView(ProductHomeNavView)
    navigationViewController.addView(ProductRecordsNavView)
    navigationViewController.addView(ProductUsersNavView)
    navigationViewController.addView(ProductWorkflowsNavView)
    navigationViewController.addView(
      ProductDocumentsNavView(createCollection, this.goTo)
    )
    navigationViewController.addView(ProductRolesNavView)
    navigationViewController.addView(ProductTasksNavView)
  }

  goTo = location => {
    this.context.router.history.push(location)
  }

  render () {
    const { children, user, isLoading } = this.props
    return (
      <div className='page-layout'>
        <ThemeProvider theme={theme => ({ ...theme, mode: globalTheme })}>
          <LayoutManagerWithViewController globalNavigation={GlobalNav}>
            <div className='main-content'>
              {isLoading ? <AppLoader /> : null}
              <Switch>
                <Route exact path='/' component={HomeContainer} />
                <Route path='/collections' component={ListCollections} />
                <Route path='/documents' component={ListDocuments} />
                <Route path='/pending-files' component={ListPendingDocuments} />
                <Route path='/document-types' component={ListDocTypes} />
                <Route path='/users' component={ListUsers} />
                <Route path='/roles' component={ListRoles} />
                <Route path='/permissions' component={ListPermissions} />
                <Route path='/workflows' component={ListWorkflows} />
                <Route path='/records' component={ListRecords} />
                <Route path='/record-types' component={ListRecordTypes} />
                <Route path='/tasks' component={ListTasks} />
                <Route path='/my-tasks' component={MyTasks} />
              </Switch>
              <ReduxToastr
                timeOut={3000}
                newestOnTop={false}
                preventDuplicates
                position='top-right'
                transitionIn='fadeIn'
                transitionOut='fadeOut'
                progressBar
                closeOnToastrClick
              />
            </div>
          </LayoutManagerWithViewController>
        </ThemeProvider>
      </div>
    )
  }
}

App.propTypes = {
  user: PropTypes.object.isRequired,
  children: PropTypes.node,
  logoutUser: PropTypes.func.isRequired
}

const wrapped = withRouter(withNavigationViewController(App))

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    isLoading: state.loading.effects.collections.create
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch.auth.logout(),
    createCollection: () => dispatch.collections.create({ name: null })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(wrapped)
