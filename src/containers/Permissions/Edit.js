import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { ResourceEditView } from 'components/Views'
import { modelConfig } from 'models/permissions'

const mapStateToProps = (state, { match }) => {
  const { resourceName, resourceForm } = modelConfig
  return {
    item: state.permissions.getIn(['list', `${match.params.id}`]),
    resourceName,
    resourceForm,
    navView: 'product/roles',
    loading: (
      state.loading.effects.permissions.fetch
    ),
    saving: state.loading.effects.permissions.update || false
  }
}

const mapDispatchToProps = (dispatch, { match }) => {
  const { id } = match.params
  return {
    saveAction: perm => dispatch.permissions.update(perm),
    prefetch: [() => dispatch.permissions.fetch({ id })]
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withCtrl(ResourceEditView)
)
