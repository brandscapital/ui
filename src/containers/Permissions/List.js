import { connect } from 'react-redux'
import { withNavigationViewController as withCtrl } from '@atlaskit/navigation-next'

import { MasterListView } from 'components/Views'
import { modelConfig } from 'models/permissions'
import EditContainer from './Edit'
import CreateContainer from './New'
import itemRenderer from './components/PermissionsListItem'

const mapStateToProps = state => {
  const { resourceName } = modelConfig
  return {
    items: state.permissions.get('list'),
    resourceName,
    detailView: EditContainer,
    createView: CreateContainer,
    itemRenderer,
    navView: 'product/roles',
    loading: (
      state.loading.effects.permissions.fetchAll
    ) || false
  }
}

const mapDispatchToProps = dispatch => {
  return {
    prefetch: [() => dispatch.permissions.fetchAll()],
    refetch: ops => dispatch.permissions.fetchAll(ops),
    removeAction: id => dispatch.permissions.delete(id)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MasterListView)
