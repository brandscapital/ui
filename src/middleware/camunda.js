import axios from 'axios'
import runtimeEnv from '@mars/heroku-js-runtime-env'

const env = runtimeEnv()

const client = axios.create({
  baseURL: env.REACT_APP_CAMUNDA_ENDPOINT,
  responseType: 'json'
})

client.defaults.headers.post['Content-Type'] = 'application/json'
client.defaults.headers.put['Content-Type'] = 'application/json'

export default client
