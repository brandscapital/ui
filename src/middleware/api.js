import axios from 'axios'
import runtimeEnv from '@mars/heroku-js-runtime-env'
import applyConverters from 'axios-case-converter'

const env = runtimeEnv()

const token = localStorage.getItem('token')
const client = applyConverters(
  axios.create({
    baseURL: env.REACT_APP_API_ENDPOINT,
    responseType: 'json'
  })
)

client.defaults.headers.post['Content-Type'] = 'application/json'
if (token) {
  client.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export default client
