import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'emotion-theming'
import { colors } from '@atlaskit/theme'
import {
  LayoutManagerWithViewController,
  NavigationProvider,
  modeGenerator,
  withNavigationViewController
} from '@atlaskit/navigation-next'
import { GlobalNav, ProductHomeNavView } from 'components/Navigation'

const globalTheme = modeGenerator({
  text: colors.N0,
  background: colors.G400
})

const productTheme = modeGenerator({
  text: colors.N0,
  background: colors.G300
})

productTheme.globalItem = globalTheme.globalItem
productTheme.globalNav = globalTheme.globalNav

class CoreLayout extends Component {
  componentDidMount () {
    const { navigationViewController } = this.props
    navigationViewController.addView(ProductHomeNavView)
  }

  render () {
    const { children, user, logoutUser } = this.props
    return (
      <div className='page-layout'>
        <ThemeProvider theme={theme => ({ ...theme, mode: productTheme })}>
          <NavigationProvider>
            <LayoutManagerWithViewController globalNavigation={GlobalNav}>
              <div className='main-content'>{children}</div>
            </LayoutManagerWithViewController>
          </NavigationProvider>
        </ThemeProvider>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  user: PropTypes.object.isRequired,
  children: PropTypes.node,
  logoutUser: PropTypes.func.isRequired
}

export default withNavigationViewController(CoreLayout)
