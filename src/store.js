import thunk from 'redux-thunk'
import { init } from '@rematch/core'
import createLoadingPlugin from '@rematch/loading'
import selectPlugin from '@rematch/select'

import * as models from 'models'

const loadingPlugin = createLoadingPlugin()

const store = init({
  models,
  plugins: [loadingPlugin, selectPlugin()],
  redux: {
    middleware: [thunk],
    devtoolOptions: {
      disabled: !process.env.NODE_ENV === 'development'
    }
  }
})

export default store
